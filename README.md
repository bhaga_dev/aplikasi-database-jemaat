# Aplikasi Database Jemaat Versi

Aplikasi Database Jemaat adalah aplikasi untuk mengelola data jemaat yang dimiliki oleh sebuah gereja. 
Aplikasi ini kami buat dengan cinta sehingga kakak dapat menggunakan aplikasi ini `secara gratis` untuk gereja kakak.

Aplikasi ini cocok digunakan oleh gereja yang berdenominasi GPIB. Walaupun demikian, kakak diperkenankan untuk mengubah / memodifikasi aplikasi ini sesuai dengan gereja kakak sendiri.

Kami juga bersedia untuk membantu jika kakak butuh tim pengembang aplikasi dalam mengembangkan aplikasi ini agar sesuai dengan kebutuhan gereja kakak.

## Fitur
- Jumlah anggota jemaat berdasarkan pelkat (pelayanan Kategorial)
- Data anggota jemaat berdasarkan pelkat (pelayanan Kategorial)
- Jumlah kepala anggota per sektor
- Jumlah anggota keluarga per sektor
- Jumlah jemaat berdasarkan usia
- Jumlah jemaat berdasarkan jenis kelamin
- Ulang tahun kelahiran jemaat
- Ulang tahun perkawinan jemaat
- Cetak Kartu Warga Jemaat. Ini adalah adalah kartu keluarga jemaat.
- Cetak Kartu Tanda Jemaat. Ini adalah kartu identitas masing-masing jemaat yang dilengkapi dengan barcode.

## Demo Program
Sabar ya kak, akan segera kami siapkan.

## Instalasi

Aplikasi Database Jemaat ini dibangun dengan berbasis web. Dengan kata lain, kakak bisa menginstall aplikasi ini di server atau web hosting agar dapat diakses secara online. 

Selain itu, aplikasi ini juga bisa berjalan secara offline di komputer lokal kakak sendiri. Berikut ini adalah panduan untuk instalasi aplikasi database jemaat untuk penggunaan secara offline: [Panduan Instalasi Aplikasi Database Jemaat](https://bitbucket.org/bhaga_dev/aplikasi-database-jemaat/wiki/)

## Ide dan Saran Pengembangan
Kami akan sangat senang jika kakak mengirimkan feedback kepada kami terkait ide dan saran untuk fitur-fitur aplikasi ini selanjutnya. Selain itu, kami terbuka untuk menerima laporan bug atau error aplikasi ini sehingga kami juga dapat memperbaikinya. 
Kakak dapat mengirimkan saran / ide pengembangan serta bug atau error aplikasi melalui di halaman berikut: [Issue Tracker](https://bitbucket.org/bhaga_dev/aplikasi-database-jemaat/issues/new)

## Developer
- Bhaga Yanuardo Missa
- Email: kotaksurat@bhaga.id
- Website: [www.bhaga.id](https://www.bhaga.id)

## Lisensi
[Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.id)
