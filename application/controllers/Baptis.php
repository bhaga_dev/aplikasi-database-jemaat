
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Baptis extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Baptis_model","baptis");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $relation[0] = array('tabel' => 'jemaat', 'relation' => 'jemaat.id_jemaat = baptis.id_jemaat', 'direction' => 'left');


                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $where = "baptis.active = 1  and jemaat.active = 1  and (baptis.id_baptis like '%".$filter."%' or baptis.id_jemaat like '%".$filter."%' or baptis.status_baptis like '%".$filter."%' or baptis.tgl_baptis like '%".$filter."%' or baptis.tempat_baptis like '%".$filter."%' or baptis.user_create like '%".$filter."%' or baptis.time_create like '%".$filter."%' or baptis.time_update like '%".$filter."%' or baptis.user_update like '%".$filter."%' )";
                $send_data = array('where' => $where, 'join' => $relation, 'limit' => $limit);
                $load_data = $this->baptis->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'join' => $relation, 'select' => $select);
                $load_data = $this->baptis->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_baptis = htmlentities($this->input->post('id_baptis'));
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$status_baptis = htmlentities($this->input->post('status_baptis'));
				$tgl_baptis = $this->reformat_date(htmlentities($this->input->post('tgl_baptis')), '-');
				$tempat_baptis = htmlentities($this->input->post('tempat_baptis'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->baptis->cek_duplikat(array('id_jemaat' => $id_jemaat));

                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'status_baptis' => $status_baptis,
								'tgl_baptis' => $tgl_baptis,
								'tempat_baptis' => $tempat_baptis,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->baptis->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'status_baptis' => $status_baptis,
								'tgl_baptis' => $tgl_baptis,
								'tempat_baptis' => $tempat_baptis,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->baptis->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
