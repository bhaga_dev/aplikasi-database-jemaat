
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hubungan_keluarga extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Hubungan_keluarga_model","hubungan_keluarga");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "hubungan_keluarga.kode_hubungan_keluarga ASC";
                $where = "hubungan_keluarga.active = 1  and (hubungan_keluarga.kode_hubungan_keluarga like '%".$filter."%' or hubungan_keluarga.nama_hubungan_keluarga like '%".$filter."%')";
                $send_data = array('where' => $where, 'limit' => $limit, 'order' => $order);
                $load_data = $this->hubungan_keluarga->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'select' => $select);
                $load_data = $this->hubungan_keluarga->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_hubungan_keluarga = htmlentities($this->input->post('id_hubungan_keluarga'));
				$kode_hubungan_keluarga = htmlentities($this->input->post('kode_hubungan_keluarga'));
				$kode_hubungan_keluarga_asli = htmlentities($this->input->post('kode_hubungan_keluarga_asli'));
				$nama_hubungan_keluarga = htmlentities($this->input->post('nama_hubungan_keluarga'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array('kode_hubungan_keluarga' => $kode_hubungan_keluarga,
								'nama_hubungan_keluarga' => $nama_hubungan_keluarga,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->hubungan_keluarga->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array('kode_hubungan_keluarga' => $kode_hubungan_keluarga,
								'nama_hubungan_keluarga' => $nama_hubungan_keluarga,
								'time_update' => $time_update,
								'user_update' => $user_update);
                    $where = array('id_hubungan_keluarga' => $id_hubungan_keluarga, 'allow_delete' => 1);
                    $exe = $this->hubungan_keluarga->update($data, $where);
                    $return['sts'] = $exe;

                    if($kode_hubungan_keluarga != $kode_hubungan_keluarga_asli){
                        $this->load->model("Jemaat_model", "jemaat");
                        $where = array('active' => 1, 'id_hubungan_keluarga' => $id_hubungan_keluarga);
                        $data_send = array('where' => $where);
                        $load_data = $this->jemaat->load_data($data_send);
                        if($load_data->num_rows() > 0){
                            foreach($load_data->result() as $row){
                                $kode_jemaat = explode('-', $row->kode_jemaat);
                                if(isset($kode_jemaat[3])){
                                    $kode_hubungan_keluarga_lama = $kode_jemaat[3];

                                    $kode_hubungan_keluarga_baru = substr_replace($kode_hubungan_keluarga_lama,$kode_hubungan_keluarga,0, 2);
                                    $kode_jemaat_baru = $kode_jemaat[0].'-'.$kode_jemaat[1].'-'.$kode_jemaat[2].'-'.$kode_hubungan_keluarga_baru;

                                    $data = array('kode_jemaat' => $kode_jemaat_baru);
                                    $where = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                                    $this->jemaat->update($data, $where);
                                }
                            }
                        }
                    }
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_hubungan_keluarga = $data_receive->id_hubungan_keluarga;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_hubungan_keluarga' => $id_hubungan_keluarga, 'allow_delete' => 1);
                $exe = $this->hubungan_keluarga->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
