
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jemaat extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Jemaat_model","jemaat");
        $this->load->model("baptis_model","baptis");
        $this->load->model("sidi_model","sidi");
        $this->load->model("perkawinan_model","perkawinan");
        $this->load->model("pendidikan_model","pendidikan");
        $this->load->model("pekerjaan_model","pekerjaan");
        $this->load->model("kontak_model","kontak");
        $this->load->model("pengalaman_keahlian_model","pengalaman_keahlian");
        $this->load->model("Lain_lain_model","lain_lain");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;
                $tipe_order = $data_receive->tipe_order; // ? $data_receive->tipe_order : 'ASC');

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $get_data = $this->data($limit, $filter, ($filter ? false : true), '', $tipe_order);
                $result = $get_data['result'];

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $get_data['where'], 'join' => $get_data['join'], 'select' => $select);
                $load_data = $this->jemaat->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    function data($limit, $filter, $kepala_keluarga_saja = false, $id_jemaat = '', $tipe_order = 'ASC'){
        $relation[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
        $relation[1] = array('tabel' => 'hubungan_keluarga', 'relation' => 'hubungan_keluarga.id_hubungan_keluarga = jemaat.id_hubungan_keluarga', 'direction' => 'left');
        $relation[2] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan', 'direction' => 'left');
        $relation[3] = array('tabel' => 'kecamatan', 'relation' => 'kelurahan.id_kecamatan = kecamatan.id_kecamatan', 'direction' => 'left');
        $relation[4] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
        $relation[5] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi = provinsi.id_provinsi', 'direction' => 'left');
        $relation[6] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');

        $order = "jemaat.kode_jemaat ASC";
        $where = "jemaat.active = 1 and (jemaat.kode_jemaat like '%".$filter."%' or jemaat.nama_jemaat like '%".$filter."%' or sektor.nama_sektor like '%".$filter."%' or hubungan_keluarga.nama_hubungan_keluarga like '%".$filter."%' or jemaat.tempat_lahir like '%".$filter."%' or jemaat.golongan_darah like '%".$filter."%' or jemaat.alamat like '%".$filter."%' or jemaat.rt like '%".$filter."%' or jemaat.rw like '%".$filter."%' or kelurahan.nama_kelurahan like '%".$filter."%')";
        if($kepala_keluarga_saja){
            $order = "jemaat.kode_jemaat ".$tipe_order;
            $where .= " and jemaat.id_hubungan_keluarga = 1";
        }
        else if(!$kepala_keluarga_saja and !$filter){
            $order = "jemaat.kode_jemaat ASC";
            $where .= " and jemaat.id_jemaat_parent = '".$id_jemaat."'";
        }
        $send_data = array('where' => $where, 'join' => $relation, 'order' => $order);
        if($limit){
            $send_data['limit'] = $limit;
        }
        $load_data = $this->jemaat->load_data($send_data);
        if($load_data->num_rows() > 0){
            foreach ($load_data->result() as $row){
                #mencari kepala keluarga
                if($row->id_jemaat_parent != 0){
                    $join_parent[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                    $where_parent = array('jemaat.active' => 1, 'id_jemaat' => $row->id_jemaat_parent);
                    $data_send_parent = array('where' => $where_parent, 'join' => $join_parent);
                    $load_data_parent = $this->jemaat->load_data($data_send_parent);
                    $row->parent = $load_data_parent->row();
                }
                else{
                    $row->parent = null;

                    #mencari anggota keluarga...
                    $row->anggota_keluarga = $this->data(null, '', false, $row->id_jemaat)['result'];
                }

                #mencari data baptis...
                $where_baptis = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_baptis = array('where' => $where_baptis);
                $load_data_baptis = $this->baptis->load_data($data_send_baptis);
                $row->baptis = $load_data_baptis->row();

                #mencari data Sidi...
                $where_sidi = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_sidi = array('where' => $where_sidi);
                $load_data_sidi = $this->sidi->load_data($data_send_sidi);
                $row->sidi = $load_data_sidi->row();

                #mencari data Perkawinan...
                $where_perkawinan = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_perkawinan = array('where' => $where_perkawinan);
                $load_data_perkawinan = $this->perkawinan->load_data($data_send_perkawinan);
                $row->perkawinan = $load_data_perkawinan->row();

                #mencari Pendidikan...
                $where_pendidikan = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_pendidikan = array('where' => $where_pendidikan);
                $load_data_pendidikan = $this->pendidikan->load_data($data_send_pendidikan);
                $row->pendidikan = $load_data_pendidikan->row();

                #mencari Pekerjaan...
                $where_pekerjaan = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_pekerjaan = array('where' => $where_pekerjaan);
                $load_data_pekerjaan = $this->pekerjaan->load_data($data_send_pekerjaan);
                $row->pekerjaan = $load_data_pekerjaan->row();

                #mencari Kontak...
                $where_kontak = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_kontak = array('where' => $where_kontak);
                $load_data_kontak = $this->kontak->load_data($data_send_kontak);
                $row->kontak = $load_data_kontak->row();

                #mencari Pengalaman & Keahlian...
                $where_pengalaman_keahlian = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_pengalaman_keahlian = array('where' => $where_pengalaman_keahlian);
                $load_data_pengalaman_keahlian = $this->pengalaman_keahlian->load_data($data_send_pengalaman_keahlian);
                $row->pengalaman_keahlian = $load_data_pengalaman_keahlian->row();

                #mencari Pengalaman & Keahlian...
                $where_lain_lain = array('active' => 1, 'id_jemaat' => $row->id_jemaat);
                $data_send_lain_lain = array('where' => $where_lain_lain);
                $load_data_lain_lain = $this->lain_lain->load_data($data_send_lain_lain);
                $row->lain_lain = $load_data_lain_lain->row();
            }
        }
        $result = $load_data->result();
        return array('result' => $result, 'where' => $where, 'join' => $relation);
    }

    public function load_select2(){
        if($this->validasi_login()){
            $token = $this->input->get('token');
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $this->input->get('filter')['term'];
                $id_hubungan_keluarga = htmlentities($this->input->get('id_hubungan_keluarga'));

                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $select = "id_jemaat id, concat(kode_jemaat, ' - ', nama_jemaat, ' (', nama_sektor, ')') text";
                $where = "jemaat.active = 1 and (jemaat.kode_jemaat like '%".$filter."%' or jemaat.nama_jemaat like '%".$filter."%' or sektor.nama_sektor like '%".$filter."%')";
                if($id_hubungan_keluarga){
                    $where .= " and jemaat.id_hubungan_keluarga = '".$id_hubungan_keluarga."'";
                }
                $data_send = array('where' => $where, 'select' => $select, 'join' => $join);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $result = $load_data->result();
                }
                echo json_encode($result);
            }
        }
    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_jemaat = htmlentities($this->input->post('id_jemaat'));
                $id_jemaat_parent = htmlentities($this->input->post('id_jemaat_parent'));
                $id_jemaat_parent_asli = htmlentities($this->input->post('id_jemaat_parent_asli'));
				$nama_jemaat = htmlentities($this->input->post('nama_jemaat'));
				$id_sektor = htmlentities($this->input->post('id_sektor'));
				$id_pelkat = htmlentities($this->input->post('id_pelkat'));
				$id_hubungan_keluarga = htmlentities($this->input->post('id_hubungan_keluarga'));
				$id_hubungan_keluarga_asli = htmlentities($this->input->post('id_hubungan_keluarga_asli'));
				$jenis_kelamin = htmlentities($this->input->post('jenis_kelamin'));
				$tgl_lahir = $this->reformat_date(htmlentities($this->input->post('tgl_lahir')), '-');
				$tempat_lahir = htmlentities($this->input->post('tempat_lahir'));
				$golongan_darah = htmlentities($this->input->post('golongan_darah'));
				$alamat = htmlentities($this->input->post('alamat'));
				$rt = htmlentities($this->input->post('rt'));
				$rw = htmlentities($this->input->post('rw'));
				$id_kelurahan = htmlentities($this->input->post('id_kelurahan'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    #create kode_jemaat...
                    $kode_jemaat = $this->create_kode_jemaat($id_sektor, $id_hubungan_keluarga, $id_jemaat_parent);
                    $data = array(
                                'kode_jemaat' => $kode_jemaat,
                                'id_jemaat_parent' => $id_jemaat_parent,
                                'nama_jemaat' => $nama_jemaat,
								'id_sektor' => $id_sektor,
								'id_pelkat' => $id_pelkat,
								'id_hubungan_keluarga' => $id_hubungan_keluarga,
								'jenis_kelamin' => $jenis_kelamin,
								'tgl_lahir' => $tgl_lahir,
								'tempat_lahir' => $tempat_lahir,
								'golongan_darah' => $golongan_darah,
								'alamat' => $alamat,
								'rt' => $rt,
								'rw' => $rw,
								'id_kelurahan' => $id_kelurahan,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->jemaat->save_with_autoincrement($data);
                        $return['sts'] = $exe['status'];
                        $return['id'] = $exe['id'];

                        $data = array('id_jemaat' => $exe['id'], 'status_jemaat' => 1);
                        $this->lain_lain->save($data);

                }
                else{
                    $data = array(
                                'id_jemaat_parent' => $id_jemaat_parent,
                                'nama_jemaat' => $nama_jemaat,
								'id_sektor' => $id_sektor,
                                'id_pelkat' => $id_pelkat,
								'id_hubungan_keluarga' => $id_hubungan_keluarga,
								'jenis_kelamin' => $jenis_kelamin,
								'tgl_lahir' => $tgl_lahir,
								'tempat_lahir' => $tempat_lahir,
								'golongan_darah' => $golongan_darah,
								'alamat' => $alamat,
								'rt' => $rt,
								'rw' => $rw,
								'id_kelurahan' => $id_kelurahan,
								'time_update' => $time_update,
								'user_update' => $user_update);

                    if($id_hubungan_keluarga_asli != $id_hubungan_keluarga){
                        $kode_jemaat = $this->create_kode_jemaat($id_sektor, $id_hubungan_keluarga, $id_jemaat_parent);
                        $data['kode_jemaat'] = $kode_jemaat;
                    }
                    else if($id_jemaat_parent_asli != $id_jemaat_parent){
                        #find id_sektor parent...
                        $where_parent = array('active' => 1, 'id_jemaat' => $id_jemaat_parent);
                        $data_send_parent = array('where' => $where_parent);
                        $load_data_parent = $this->jemaat->load_data($data_send_parent);
                        if($load_data_parent->num_rows() > 0){
                            $parent = $load_data_parent->row();
                            $id_sektor = $parent->id_sektor;
                            $data['id_sektor'] = $id_sektor;
                        }

                        $kode_jemaat = $this->create_kode_jemaat($id_sektor, $id_hubungan_keluarga, $id_jemaat_parent);
                        $data['kode_jemaat'] = $kode_jemaat;
                    }

                    $where = array('id_jemaat' => $id_jemaat);
                    $exe = $this->jemaat->update($data, $where);

                    $return['sts'] = $exe;
                    $return['id'] = $id_jemaat;
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_jemaat = $data_receive->id_jemaat;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_jemaat' => $id_jemaat);
                $exe = $this->jemaat->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }
    public function hapus_ganti_kk(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_jemaat_parent_lama = $data_receive->id_jemaat_parent_lama;
            $id_jemaat_parent_baru = $data_receive->id_jemaat_parent_baru;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_jemaat' => $id_jemaat_parent_lama);
                $exe = $this->jemaat->soft_delete($where);
                $return['sts'] = $exe;

                if($exe){
                    $where = array('active' => 1, 'id_jemaat' => $id_jemaat_parent_baru);
                    $data_send = array('where' => $where);
                    $load_data = $this->jemaat->load_data($data_send);
                    if($load_data->num_rows() > 0){
                        $jemaat = $load_data->row();

                        $kode_jemaat = $this->create_kode_jemaat($jemaat->id_sektor, 1, 0, true);
                        $data = array('id_hubungan_keluarga' => 1,
                            'kode_jemaat' => $kode_jemaat,
                            'id_jemaat_parent' => 0,
                            );
                        $where = array('id_jemaat' => $id_jemaat_parent_baru);
                        $this->jemaat->update($data, $where);
                    }
                }
            }

            echo json_encode($return);
        }
    }


    public function profil_jemaat(){
        if($this->validasi_login()){
            $kode_jemaat = htmlentities($this->input->get('kode_jemaat'));
            $token = $this->input->get('token');
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $join[1] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan ', 'direction' => 'left');
                $join[2] = array('tabel' => 'kecamatan', 'relation' => 'kelurahan.id_kecamatan = kecamatan.id_kecamatan', 'direction' => 'left');
                $join[3] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $join[4] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi = provinsi.id_provinsi', 'direction' => 'left');
                $join[5] = array('tabel' => 'hubungan_keluarga', 'relation' => 'hubungan_keluarga.id_hubungan_keluarga = jemaat.id_hubungan_keluarga', 'direction' => 'left');
                $join[6] = array('tabel' => 'jemaat_baptis', 'relation' => 'jemaat_baptis.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[7] = array('tabel' => 'jemaat_kontak', 'relation' => 'jemaat_kontak.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[8] = array('tabel' => 'jemaat_lain_lain', 'relation' => 'jemaat_lain_lain.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[9] = array('tabel' => 'jemaat_pekerjaan', 'relation' => 'jemaat_pekerjaan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[10] = array('tabel' => 'jemaat_pendidikan', 'relation' => 'jemaat_pendidikan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[11] = array('tabel' => 'jemaat_pengalaman_keahlian', 'relation' => 'jemaat_pengalaman_keahlian.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[12] = array('tabel' => 'jemaat_perkawinan', 'relation' => 'jemaat_perkawinan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[13] = array('tabel' => 'jemaat_sidi', 'relation' => 'jemaat_sidi.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[14] = array('tabel' => 'status_perkawinan', 'relation' => 'status_perkawinan.id_status_perkawinan = jemaat_perkawinan.id_status_perkawinan', 'direction' => 'left');
                $join[15] = array('tabel' => 'jenjang_pendidikan', 'relation' => 'jenjang_pendidikan.id_jenjang_pendidikan = jemaat_pendidikan.id_jenjang_pendidikan', 'direction' => 'left');
                $join[16] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');

                $where = "jemaat.active = 1 and (jemaat.id_jemaat = '".$kode_jemaat."' or kode_jemaat = '".$kode_jemaat."')";
                $data_send = array('where' => $where, 'join' => $join);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $jemaat = $load_data->row();
                    $konten = array('jemaat' => $jemaat);
                    $this->load->view('laporan/profil_jemaat_print', $this->data_halaman($konten));
                }
                else{
                    $this->load->view('jemaat/jemaat_tidak_ada', $this->data_halaman());
                }
            }
            else{
                $this->load->view('jemaat/jemaat_tidak_ada', $this->data_halaman());
            }
        }
    }


    public function ganti_background_kartu(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')) {

                $desain_background_blob = $this->input->post('desain_background_blob');

                $file_foto = '';
                $exe = false;
                if ($desain_background_blob != '') {
                    $output = 'assets/uploads/kartu/';
                    $filename = 'background_kartu.jpg';
                    $file_foto = $this->base64_to_file($desain_background_blob, $output, $filename);
                    $exe = true;
                }

                $return['sts'] = $exe;
            }
            echo json_encode($return);
        }
    }

}
