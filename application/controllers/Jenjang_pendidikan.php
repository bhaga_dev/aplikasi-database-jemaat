
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenjang_pendidikan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Jenjang_pendidikan_model","jenjang_pendidikan");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;


                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "jenjang_pendidikan.id_jenjang_pendidikan DESC";
                $where = "jenjang_pendidikan.active = 1  and (jenjang_pendidikan.nama_jenjang_pendidikan like '%".$filter."%')";
                $send_data = array('where' => $where, 'limit' => $limit, 'order' => $order);
                $load_data = $this->jenjang_pendidikan->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'select' => $select);
                $load_data = $this->jenjang_pendidikan->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_jenjang_pendidikan = htmlentities($this->input->post('id_jenjang_pendidikan'));
				$nama_jenjang_pendidikan = htmlentities($this->input->post('nama_jenjang_pendidikan'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array(								'nama_jenjang_pendidikan' => $nama_jenjang_pendidikan,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->jenjang_pendidikan->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(								'nama_jenjang_pendidikan' => $nama_jenjang_pendidikan,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jenjang_pendidikan' => $id_jenjang_pendidikan);
                        $exe = $this->jenjang_pendidikan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_jenjang_pendidikan = $data_receive->id_jenjang_pendidikan;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_jenjang_pendidikan' => $id_jenjang_pendidikan);
                $exe = $this->jenjang_pendidikan->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
