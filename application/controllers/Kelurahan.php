
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelurahan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Kelurahan_model","kelurahan");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $relation[0] = array('tabel' => 'kecamatan', 'relation' => 'kecamatan.id_kecamatan = kelurahan.id_kecamatan', 'direction' => 'left');
                $relation[1] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $relation[2] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi= provinsi.id_provinsi', 'direction' => 'left');

                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "kelurahan.id_kelurahan DESC";
                $where = "kelurahan.active = 1  and kecamatan.active = 1  and (provinsi.nama_provinsi like '%".$filter."%' or kabupaten_kota.nama_kabupaten_kota like '%".$filter."%' or kecamatan.nama_kecamatan like '%".$filter."%' or kelurahan.nama_kelurahan like '%".$filter."%')";
                $send_data = array('where' => $where, 'join' => $relation, 'limit' => $limit, 'order' => $order);
                $load_data = $this->kelurahan->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'join' => $relation, 'select' => $select);
                $load_data = $this->kelurahan->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function load_select2(){
        if($this->validasi_login()){
            $token = $this->input->get('token');
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $this->input->get('filter')['term'];

                $join[0] = array('tabel' => 'kecamatan', 'relation' => 'kecamatan.id_kecamatan = kelurahan.id_kecamatan', 'direction' => 'left');
                $join[1] = array('tabel' => 'kabupaten_kota', 'relation' => 'kecamatan.id_kabupaten_kota = kabupaten_kota.id_kabupaten_kota', 'direction' => 'left');
                $join[2] = array('tabel' => 'provinsi', 'relation' => 'provinsi.id_provinsi = kabupaten_kota.id_provinsi', 'direction' => 'left');
                $select = "id_kelurahan id, concat(nama_kelurahan, ' - ', nama_kecamatan, ' - ', nama_kabupaten_kota, ' - ', nama_provinsi) text";
                $where = "kelurahan.active = 1 and (kelurahan.nama_kelurahan like '%".$filter."%' or kecamatan.nama_kecamatan like '%".$filter."%' or kabupaten_kota.nama_kabupaten_kota like '%".$filter."%' or provinsi.nama_provinsi like '%".$filter."%')";

                $data_send = array('where' => $where, 'select' => $select, 'join' => $join);
                $load_data = $this->kelurahan->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $result = $load_data->result();
                }
                echo json_encode($result);
            }
        }
    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_kelurahan = htmlentities($this->input->post('id_kelurahan'));
				$id_kecamatan = htmlentities($this->input->post('id_kecamatan'));
				$nama_kelurahan = htmlentities($this->input->post('nama_kelurahan'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array(								'id_kecamatan' => $id_kecamatan,
								'nama_kelurahan' => $nama_kelurahan,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->kelurahan->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(								'id_kecamatan' => $id_kecamatan,
								'nama_kelurahan' => $nama_kelurahan,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_kelurahan' => $id_kelurahan);
                        $exe = $this->kelurahan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_kelurahan = $data_receive->id_kelurahan;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_kelurahan' => $id_kelurahan);
                $exe = $this->kelurahan->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
