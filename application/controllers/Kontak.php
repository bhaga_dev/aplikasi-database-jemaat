
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Kontak_model","kontak");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$telepon_rumah = htmlentities($this->input->post('telepon_rumah'));
				$nomor_hp = htmlentities($this->input->post('nomor_hp'));
				$nomor_hp = str_replace('_', '', $nomor_hp);
				$nomor_hp = str_replace(' ', '', $nomor_hp);
				$email = htmlentities($this->input->post('email'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->kontak->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'telepon_rumah' => $telepon_rumah,
								'nomor_hp' => $nomor_hp,
								'email' => $email,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->kontak->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'telepon_rumah' => $telepon_rumah,
								'nomor_hp' => $nomor_hp,
								'email' => $email,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->kontak->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
