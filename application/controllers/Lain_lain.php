
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lain_lain extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Lain_lain_model","lain_lain");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$riwayat = htmlentities($this->input->post('riwayat'));
				$catatan = htmlentities($this->input->post('catatan'));
				$status_jemaat = htmlentities($this->input->post('status_jemaat'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->lain_lain->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'riwayat' => $riwayat,
								'catatan' => $catatan,
								'status_jemaat' => $status_jemaat,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->lain_lain->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'riwayat' => $riwayat,
								'catatan' => $catatan,
								'status_jemaat' => $status_jemaat,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->lain_lain->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
