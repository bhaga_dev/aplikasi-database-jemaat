<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {


    function __construct(){
        parent::__construct();
    }

    function lost(){
        //halaman tidak ditemukan...
        $this->load->view('errors/404', $this->data_halaman());

    }
    public function index(){
        $this->load->view('admin_login', $this->data_halaman());
    }

    public function home(){
        $menu = 'page/home';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('welcome', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function hubungan_keluarga(){
        $menu = 'page/hubungan_keluarga';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('master/hubungan_keluarga', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function profil_gereja(){
        $menu = 'page/profil_gereja';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('gereja/profil_gereja', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function sektor(){
        $menu = 'page/sektor';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('gereja/sektor', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function pelkat(){
        $menu = 'page/pelkat';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('gereja/pelkat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function jemaat(){
        $menu = 'page/jemaat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('sektor_model', 'sektor');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $sektor = $this->sektor->load_data($send_data);

            $this->load->model('hubungan_keluarga_model', 'hubungan_keluarga');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $hubungan_keluarga = $this->hubungan_keluarga->load_data($send_data);

            $this->load->model('pelkat_model', 'pelkat');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $pelkat = $this->pelkat->load_data($send_data);

            $this->load->model('status_perkawinan_model', 'status_perkawinan');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $status_perkawinan = $this->status_perkawinan->load_data($send_data);

            $this->load->model('jenjang_pendidikan_model', 'jenjang_pendidikan');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $jenjang_pendidikan = $this->jenjang_pendidikan->load_data($send_data);

            $konten = array(
                'sektor' => $sektor,
                'pelkat' => $pelkat,
                'hubungan_keluarga' => $hubungan_keluarga,
                'status_perkawinan' => $status_perkawinan,
                'jenjang_pendidikan' => $jenjang_pendidikan,
            );
            $this->load->view('jemaat/jemaat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function provinsi(){
        $menu = 'page/provinsi';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('wilayah/provinsi', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function kabupaten_kota(){
        $menu = 'page/kabupaten_kota';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('provinsi_model', 'provinsi');
            $order = "nama_provinsi ASC";
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where, 'order' => $order);
            $provinsi = $this->provinsi->load_data($send_data);

            $konten = array('provinsi' => $provinsi);
            $this->load->view('wilayah/kabupaten_kota', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function kecamatan(){
        $menu = 'page/kecamatan';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kabupaten_kota_model', 'kabupaten_kota');
            $order = "provinsi.nama_provinsi ASC, kabupaten_kota.nama_kabupaten_kota ASC";
            $join[0] = array('tabel' => 'provinsi', 'relation' => 'provinsi.id_provinsi = kabupaten_kota.id_provinsi', 'direction' => 'left');
            $where = array('kabupaten_kota.active' => 1);  #show active data...
            $send_data = array('where' => $where, 'join' => $join, 'order' => $order);
            $kabupaten_kota = $this->kabupaten_kota->load_data($send_data);

            $konten = array('kabupaten_kota' => $kabupaten_kota);
            $this->load->view('wilayah/kecamatan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function kelurahan(){
        $menu = 'page/kelurahan';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kecamatan_model', 'kecamatan');
            $order = "provinsi.nama_provinsi ASC, kabupaten_kota.nama_kabupaten_kota ASC, kecamatan.nama_kecamatan ASC";
            $join[0] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
            $join[1] = array('tabel' => 'provinsi', 'relation' => 'provinsi.id_provinsi = kabupaten_kota.id_provinsi', 'direction' => 'left');
            $where = array('kecamatan.active' => 1);  #show active data...
            $send_data = array('where' => $where, 'join' => $join, 'order' => $order);
            $kecamatan = $this->kecamatan->load_data($send_data);

            $konten = array('kecamatan' => $kecamatan);
            $this->load->view('wilayah/kelurahan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function jenjang_pendidikan(){
        $menu = 'page/jenjang_pendidikan';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('master/jenjang_pendidikan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function status_perkawinan(){
        $menu = 'page/status_perkawinan';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('master/status_perkawinan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function summary(){
        $menu = 'page/summary';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('laporan/summary', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function ultah_lahir(){
        $menu = 'page/ultah_lahir';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('laporan/ultah_lahir', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function ultah_kawin(){
        $menu = 'page/ultah_kawin';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('laporan/ultah_kawin', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function cetak_kwj(){
        $menu = 'page/cetak_kwj';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $konten = array();
            $this->load->view('jemaat/kwj', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function cetak_kartu_jemaat(){
        $menu = 'page/cetak_kartu_jemaat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $konten = array();
            $this->load->view('jemaat/kartu_jemaat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function background_kartu(){
        $menu = 'page/cetak_kartu_jemaat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $konten = array();
            $this->load->view('jemaat/ganti_background_kartu', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function anggota_pelkat(){
        $menu = 'page/anggota_pelkat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model("Pelkat_model", "pelkat");
            $where = array('active' => 1);
            $data_send = array('where' => $where);
            $pelkat = $this->pelkat->load_data($data_send);

            $this->load->model("Sektor_model", "sektor");
            $where = array('active' => 1);
            $data_send = array('where' => $where);
            $sektor = $this->sektor->load_data($data_send);


            $konten = array('pelkat' => $pelkat, 'sektor' => $sektor);
            $this->load->view('laporan/anggota_pelkat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function tipe_admin(){
        $menu = 'page/tipe_admin';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('setting/jns_admin', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function hak_akses_menu(){
        $menu = 'page/hak_akses_menu';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();
            $konten = array('jns_admin' => $load_jns_admin);

            $this->load->view('setting/hak_akses_menu', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function hak_akses_status(){
        $menu = 'page/hak_akses_status';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();

            #menampilkan status pasien...
            $this->load->model('Status_pasien_model', 'status_pasien');
            $load_status_pasien = $this->status_pasien->load_data();
            $konten = array('jns_admin' => $load_jns_admin,
                'status_pasien' => $load_status_pasien);

            $this->load->view('setting/hak_akses_status', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function administrator(){
        $menu = 'page/administrator';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();


            $konten = array('jns_admin' => $load_jns_admin);

            $this->load->view('setting/administrator', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function ganti_password(){
        $menu = 'page/ganti_password';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('setting/ganti_password', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');

    }

    public function generate_image(){
        function getImage_w($image,$w, $h='')
        {
            $source_path = $image;
            $w *= 1.3;
            $h *= 1.3;

            //Add file validation code here
            list($source_width, $source_height, $source_type) = getimagesize($source_path);

            if($h == '' or $h == 0)
                define('DESIRED_IMAGE_HEIGHT', round(($w/ $source_width) * $source_height));
            else
                define('DESIRED_IMAGE_HEIGHT', $h);

            define('DESIRED_IMAGE_WIDTH', $w);

            switch ($source_type) {
                case IMAGETYPE_GIF:
                    $source_gdim = imagecreatefromgif($source_path);
                    break;
                case IMAGETYPE_JPEG:
                    $source_gdim = imagecreatefromjpeg($source_path);
                    break;
                case IMAGETYPE_PNG:
                    $source_gdim = imagecreatefrompng($source_path);
                    break;
            }

            $source_aspect_ratio = $source_width / $source_height;
            $desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;

            if ($source_aspect_ratio > $desired_aspect_ratio) {
                /*
                 * Triggered when source image is wider
                 */
                $temp_height = DESIRED_IMAGE_HEIGHT;
                $temp_width = ( int ) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
            } else {
                /*
                 * Triggered otherwise (i.e. source image is similar or taller)
                 */
                $temp_width = DESIRED_IMAGE_WIDTH;
                $temp_height = ( int ) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
            }

            /*
             * Resize the image into a temporary GD image
             */

            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $temp_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );

            /*
             * Copy cropped region from temporary image into the desired GD image
             */

            $x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
            $y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
            $desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
            imagecopy(
                $desired_gdim,
                $temp_gdim,
                0, 0,
                $x0, $y0,
                DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
            );

            /*
             * Render the image
             * Alternatively, you can save the image in file-system or database
             */

            header('Content-type: image/jpeg');
            imagejpeg($desired_gdim, null, 100);

        }

        $path = $this->input->get('path');
        $width = $this->input->get('width');
        $height = $this->input->get('height');
        echo '<img src="data:image/jpeg;base64,' . base64_encode(getImage_w($path,$width,$height)) . '">';
    }
}
