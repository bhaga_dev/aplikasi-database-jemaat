
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pekerjaan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Pekerjaan_model","pekerjaan");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_pekerjaan = htmlentities($this->input->post('id_pekerjaan'));
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$nama_pekerjaan = htmlentities($this->input->post('nama_pekerjaan'));
				$kantor_institusi = htmlentities($this->input->post('kantor_institusi'));
				$posisi_jabatan = htmlentities($this->input->post('posisi_jabatan'));
				$profesi = htmlentities($this->input->post('profesi'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->pekerjaan->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'nama_pekerjaan' => $nama_pekerjaan,
								'kantor_institusi' => $kantor_institusi,
								'posisi_jabatan' => $posisi_jabatan,
								'profesi' => $profesi,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->pekerjaan->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'nama_pekerjaan' => $nama_pekerjaan,
								'kantor_institusi' => $kantor_institusi,
								'posisi_jabatan' => $posisi_jabatan,
								'profesi' => $profesi,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->pekerjaan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
