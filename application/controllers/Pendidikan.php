
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Pendidikan_model","pendidikan");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$id_jenjang_pendidikan = htmlentities($this->input->post('id_jenjang_pendidikan'));
				$jurusan_pendidikan = htmlentities($this->input->post('jurusan_pendidikan'));
				$gelar = htmlentities($this->input->post('gelar'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->pendidikan->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'id_jenjang_pendidikan' => $id_jenjang_pendidikan,
								'jurusan_pendidikan' => $jurusan_pendidikan,
								'gelar' => $gelar,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->pendidikan->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'id_jenjang_pendidikan' => $id_jenjang_pendidikan,
								'jurusan_pendidikan' => $jurusan_pendidikan,
								'gelar' => $gelar,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->pendidikan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
