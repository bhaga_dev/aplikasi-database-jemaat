
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengalaman_keahlian extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Pengalaman_keahlian_model","pengalaman_keahlian");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$pengalaman_gereja = htmlentities($this->input->post('pengalaman_gereja'));
				$penguasaan_bahasa = htmlentities($this->input->post('penguasaan_bahasa'));
				$keahlian = htmlentities($this->input->post('keahlian'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->pengalaman_keahlian->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'pengalaman_gereja' => $pengalaman_gereja,
								'penguasaan_bahasa' => $penguasaan_bahasa,
								'keahlian' => $keahlian,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->pengalaman_keahlian->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'pengalaman_gereja' => $pengalaman_gereja,
								'penguasaan_bahasa' => $penguasaan_bahasa,
								'keahlian' => $keahlian,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->pengalaman_keahlian->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
