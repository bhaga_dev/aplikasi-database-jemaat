
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perkawinan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Perkawinan_model","perkawinan");
	}

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
				$id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$id_status_perkawinan = htmlentities($this->input->post('id_status_perkawinan'));
				$tgl_kawin_gereja = $this->reformat_date(htmlentities($this->input->post('tgl_kawin_gereja')), '-');
				$tgl_kawin_sipil = $this->reformat_date(htmlentities($this->input->post('tgl_kawin_sipil')), '-');
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->perkawinan->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array('id_jemaat' => $id_jemaat,
								'id_status_perkawinan' => $id_status_perkawinan,
								'tgl_kawin_gereja' => $tgl_kawin_gereja,
								'tgl_kawin_sipil' => $tgl_kawin_sipil,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->perkawinan->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
								'id_status_perkawinan' => $id_status_perkawinan,
								'tgl_kawin_gereja' => $tgl_kawin_gereja,
								'tgl_kawin_sipil' => $tgl_kawin_sipil,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->perkawinan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
