
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil_gereja extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Profil_gereja_model","profil_gereja");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $where = array('profil_gereja.active' => 1, 'profil_gereja.id_profil_gereja' => 1); #show active data...

                $send_data = array('where' => $where);
                $load_data = $this->profil_gereja->load_data($send_data);
                $result = $load_data->row();

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_profil_gereja = htmlentities($this->input->post('id_profil_gereja'));
                $kode_gereja = htmlentities($this->input->post('kode_gereja'));
                $kode_gereja_asli = htmlentities($this->input->post('kode_gereja_asli'));
				$nama_gereja = htmlentities($this->input->post('nama_gereja'));
				$alamat_gereja = htmlentities($this->input->post('alamat_gereja'));
				$nomor_telepon_gereja = htmlentities($this->input->post('nomor_telepon_gereja'));
				$logo_gereja_blob = $this->input->post('logo_gereja_blob');
				$kmj = htmlentities($this->input->post('kmj'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

				$cek = $this->profil_gereja->cek_duplikat(array('id_profil_gereja' => 1));

                $logo_gereja = '';
                if($logo_gereja_blob != ''){
                    $new_name = date('ymdHis').'_'.$this->generateRandomString(5);
                    $output = 'assets/uploads/logo_gereja/';
                    $filename = $new_name.'.jpg';
                    $logo_gereja = $this->base64_to_file($logo_gereja_blob, $output, $filename);
                }

                if(!$cek){
                    $data = array(
                                'id_profil_gereja' => 1,
                                'kode_gereja' => $kode_gereja,
                                'nama_gereja' => $nama_gereja,
								'alamat_gereja' => $alamat_gereja,
								'nomor_telepon_gereja' => $nomor_telepon_gereja,
								'logo_gereja' => $logo_gereja,
								'kmj' => $kmj,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->profil_gereja->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array(
                                'kode_gereja' => $kode_gereja,
                                'nama_gereja' => $nama_gereja,
								'alamat_gereja' => $alamat_gereja,
								'nomor_telepon_gereja' => $nomor_telepon_gereja,
								'kmj' => $kmj,
								'time_update' => $time_update,
								'user_update' => $user_update);
                    if($logo_gereja){
                        $data['logo_gereja'] = $logo_gereja;
                    }
                    $where = array('id_profil_gereja' => 1);
                    $exe = $this->profil_gereja->update($data, $where);
                    $return['sts'] = $exe;

                    if($kode_gereja != $kode_gereja_asli){
                        #update semua kode jemaat sesuai kode sektor...
                        $str = "UPDATE `jemaat` SET kode_jemaat = REPLACE(kode_jemaat, '".$kode_gereja_asli."', '".$kode_gereja."') WHERE `active` = 1";
                        $this->profil_gereja->query($str);
                    }
                }
            }

            echo json_encode($return);
        }
    }
}
