
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sektor extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Sektor_model","sektor");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "sektor.nama_sektor ASC";
                $where = "sektor.active = 1  and (sektor.kode_sektor like '%".$filter."%' or sektor.nama_sektor like '%".$filter."%' or sektor.koordinator_sektor like '%".$filter."%')";
                $send_data = array('where' => $where, 'limit' => $limit, 'order' => $order);
                $load_data = $this->sektor->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'select' => $select);
                $load_data = $this->sektor->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_sektor = htmlentities($this->input->post('id_sektor'));
				$kode_sektor_asli = htmlentities($this->input->post('kode_sektor_asli'));
				$kode_sektor = htmlentities($this->input->post('kode_sektor'));
				$nama_sektor = htmlentities($this->input->post('nama_sektor'));
				$koordinator_sektor = htmlentities($this->input->post('koordinator_sektor'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array('kode_sektor' => $kode_sektor,
								'nama_sektor' => $nama_sektor,
								'koordinator_sektor' => $koordinator_sektor,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->sektor->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array('kode_sektor' => $kode_sektor,
								'nama_sektor' => $nama_sektor,
								'koordinator_sektor' => $koordinator_sektor,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_sektor' => $id_sektor);
                        $exe = $this->sektor->update($data, $where);
                        $return['sts'] = $exe;

                        if($kode_sektor_asli != $kode_sektor){
                            #update semua kode jemaat sesuai kode sektor...
                            $str = "UPDATE `jemaat` SET kode_jemaat = REPLACE(kode_jemaat, '".$kode_sektor_asli."', '".$kode_sektor."') WHERE `active` = 1 and `id_sektor` = '".$id_sektor."'";
                            $this->sektor->query($str);
                        }
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_sektor = $data_receive->id_sektor;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_sektor' => $id_sektor);
                $exe = $this->sektor->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
