
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sidi extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Sidi_model","sidi");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;

                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $where = "sidi.active = 1  and (sidi.id_sidi like '%".$filter."%' or sidi.status_sidi like '%".$filter."%' or sidi.tgl_sidi like '%".$filter."%' or sidi.tempat_sidi like '%".$filter."%' or sidi.user_create like '%".$filter."%' or sidi.time_create like '%".$filter."%' or sidi.time_update like '%".$filter."%' or sidi.user_update like '%".$filter."%' )";
                $send_data = array('where' => $where, 'limit' => $limit);
                $load_data = $this->sidi->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'select' => $select);
                $load_data = $this->sidi->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_jemaat = htmlentities($this->input->post('id_jemaat'));
				$status_sidi = htmlentities($this->input->post('status_sidi'));
				$tgl_sidi = $this->reformat_date(htmlentities($this->input->post('tgl_sidi')), '-');
				$tempat_sidi = htmlentities($this->input->post('tempat_sidi'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $cek_jemaat = $this->sidi->cek_duplikat(array('id_jemaat' => $id_jemaat));
                if(!$cek_jemaat){
                    $data = array(
                                'id_jemaat' => $id_jemaat,
                                'status_sidi' => $status_sidi,
								'tgl_sidi' => $tgl_sidi,
								'tempat_sidi' => $tempat_sidi,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->sidi->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array('status_sidi' => $status_sidi,
								'tgl_sidi' => $tgl_sidi,
								'tempat_sidi' => $tempat_sidi,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_jemaat' => $id_jemaat);
                        $exe = $this->sidi->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
}
