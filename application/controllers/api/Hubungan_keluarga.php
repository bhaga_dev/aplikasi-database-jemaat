<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Hubungan_keluarga extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Hubungan_keluarga_model",'hubungan_keluarga');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));

        $id_hubungan_keluarga = htmlentities($this->input->post('id_hubungan_keluarga'));
        $nama_hubungan_keluarga = htmlentities($this->input->post('nama_hubungan_keluarga'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $order = ' id_hubungan_keluarga ASC';

                $where = "active = 1";
                if($id_hubungan_keluarga != ''){
                    $where .= " and id_hubungan_keluarga IN (".$id_hubungan_keluarga.")";
                }
                if($nama_hubungan_keluarga != ''){
                    $where .= " and nama_hubungan_keluarga like '%".$nama_hubungan_keluarga."%'";
                }

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->hubungan_keluarga->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = '';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
