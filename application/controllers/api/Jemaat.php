<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Jemaat extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Jemaat_model",'jemaat');
	}

    public function individu_jemaat(){
        $token = htmlentities($this->input->post('token'));
        $page = htmlentities($this->input->post('page'));
        $jml_data = htmlentities($this->input->post('jml_data'));
        $id_jemaat = htmlentities($this->input->post('id_jemaat'));
        $kode_jemaat = htmlentities($this->input->post('kode_jemaat'));
        $id_sektor = htmlentities($this->input->post('id_sektor'));
        $id_kelurahan = htmlentities($this->input->post('id_kelurahan'));
        $id_kecamatan = htmlentities($this->input->post('id_kecamatan'));
        $id_kabupaten_kota = htmlentities($this->input->post('id_kabupaten_kota'));
        $id_provinsi = htmlentities($this->input->post('id_provinsi'));
        $id_hubungan_keluarga = htmlentities($this->input->post('id_hubungan_keluarga'));
        $status_baptis = htmlentities($this->input->post('status_baptis'));
        $id_jenjang_pendidikan = htmlentities($this->input->post('id_jenjang_pendidikan'));
        $id_status_perkawinan = htmlentities($this->input->post('id_status_perkawinan'));
        $status_sidi = htmlentities($this->input->post('status_sidi'));
        $id_pelkat = htmlentities($this->input->post('id_pelkat'));
        $pencarian = htmlentities($this->input->post('pencarian'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $jml_data = ($jml_data ? $jml_data : $this->qty_data_mobile);
                $page = ($page ? $page : 1);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = ' jemaat.id_jemaat ASC';

                $where = "jemaat.active = 1";
                if($id_jemaat != ''){
                    $where .= " and jemaat.id_jemaat = '".$id_jemaat."'";
                }
                if($kode_jemaat != ''){
                    $where .= " and jemaat.kode_jemaat = '".$kode_jemaat."'";
                }
                if($id_sektor != ''){
                    $where .= " and sektor.id_sektor = '".$id_sektor."'";
                }
                if($id_kelurahan != ''){
                    $where .= " and kelurahan.id_kelurahan = '".$id_kelurahan."'";
                }
                if($id_kecamatan != ''){
                    $where .= " and kecamatan.id_kecamatan = '".$id_kecamatan."'";
                }
                if($id_kabupaten_kota != ''){
                    $where .= " and kabupaten_kota.id_kabupaten_kota = '".$id_kabupaten_kota."'";
                }
                if($id_provinsi != ''){
                    $where .= " and provinsi.id_provinsi = '".$id_provinsi."'";
                }
                if($id_hubungan_keluarga != ''){
                    $where .= " and hubungan_keluarga.id_hubungan_keluarga = '".$id_hubungan_keluarga."'";
                }
                if($status_baptis != ''){
                    $where .= " and jemaat_baptis.status_baptis = '".$status_baptis."'";
                }
                if($id_jenjang_pendidikan != ''){
                    $where .= " and jenjang_pendidikan.id_jenjang_pendidikan = '".$id_jenjang_pendidikan."'";
                }
                if($id_status_perkawinan != ''){
                    $where .= " and status_perkawinan.id_status_perkawinan = '".$id_status_perkawinan."'";
                }
                if($status_sidi != ''){
                    $where .= " and jemaat_sidi.status_sidi = '".$status_sidi."'";
                }
                if($id_pelkat != ''){
                    $where .= " and pelkat.id_pelkat IN (".$id_pelkat.")";
                }
                if($pencarian != ''){
                    $where .= " and (jemaat.kode_jemaat = '".$pencarian."' or nama_jemaat like '%".$pencarian."%')";
                }

                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $join[1] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan ', 'direction' => 'left');
                $join[2] = array('tabel' => 'kecamatan', 'relation' => 'kelurahan.id_kecamatan = kecamatan.id_kecamatan', 'direction' => 'left');
                $join[3] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $join[4] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi = provinsi.id_provinsi', 'direction' => 'left');
                $join[5] = array('tabel' => 'hubungan_keluarga', 'relation' => 'hubungan_keluarga.id_hubungan_keluarga = jemaat.id_hubungan_keluarga', 'direction' => 'left');
                $join[6] = array('tabel' => 'jemaat_baptis', 'relation' => 'jemaat_baptis.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[7] = array('tabel' => 'jemaat_kontak', 'relation' => 'jemaat_kontak.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[8] = array('tabel' => 'jemaat_lain_lain', 'relation' => 'jemaat_lain_lain.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[9] = array('tabel' => 'jemaat_pekerjaan', 'relation' => 'jemaat_pekerjaan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[10] = array('tabel' => 'jemaat_pendidikan', 'relation' => 'jemaat_pendidikan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[11] = array('tabel' => 'jemaat_pengalaman_keahlian', 'relation' => 'jemaat_pengalaman_keahlian.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[12] = array('tabel' => 'jemaat_perkawinan', 'relation' => 'jemaat_perkawinan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[13] = array('tabel' => 'jemaat_sidi', 'relation' => 'jemaat_sidi.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[14] = array('tabel' => 'status_perkawinan', 'relation' => 'status_perkawinan.id_status_perkawinan = jemaat_perkawinan.id_status_perkawinan', 'direction' => 'left');
                $join[15] = array('tabel' => 'jenjang_pendidikan', 'relation' => 'jenjang_pendidikan.id_jenjang_pendidikan = jemaat_pendidikan.id_jenjang_pendidikan', 'direction' => 'left');
                $join[16] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');

                $select = "*, jemaat.id_jemaat id_jemaat_asli";
                $data_send = array('where' => $where, 'order' => $order, 'limit' => $limit, 'join' => $join, 'select' => $select);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    foreach ($load_data->result() as $jemaat){
                        unset($jemaat->active);
                        unset($jemaat->user_create);
                        unset($jemaat->user_update);
                        unset($jemaat->time_create);
                        unset($jemaat->time_update);
                        unset($jemaat->id_jemaat);
                        $jemaat->id_jemaat = $jemaat->id_jemaat_asli;
                        unset($jemaat->id_jemaat_asli);
                    }
                }
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }
    public function jml_individu_jemaat(){
        $token = htmlentities($this->input->post('token'));
        $id_sektor = htmlentities($this->input->post('id_sektor'));
        $id_pelkat = htmlentities($this->input->post('id_pelkat'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $where = "jemaat.active = 1";

                if($id_sektor != ''){
                    $where .= " and id_sektor = '".$id_sektor."'";
                }
                if($id_pelkat != ''){
                    $where .= " and id_pelkat IN (".$id_pelkat.")";
                }

                $jml_jemaat = $this->jemaat->select_count($where);

                $return['sts'] = true;
                $return['data'] = $jml_jemaat;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

    public function anggota_keluarga_jemaat(){
        $token = htmlentities($this->input->post('token'));
        $page = htmlentities($this->input->post('page'));
        $jml_data = htmlentities($this->input->post('jml_data'));
        $id_jemaat = htmlentities($this->input->post('id_kepala_keluarga'));
        $kode_jemaat = htmlentities($this->input->post('kode_jemaat_kepala_keluarga'));
        $id_sektor = htmlentities($this->input->post('id_sektor'));
        $id_kelurahan = htmlentities($this->input->post('id_kelurahan'));
        $id_kecamatan = htmlentities($this->input->post('id_kecamatan'));
        $id_kabupaten_kota = htmlentities($this->input->post('id_kabupaten_kota'));
        $id_provinsi = htmlentities($this->input->post('id_provinsi'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $jml_data = ($jml_data ? $jml_data : $this->qty_data_mobile);
                $page = ($page ? $page : 1);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = ' jemaat.id_jemaat ASC';

                $where = "jemaat.active = 1 and hubungan_keluarga.id_hubungan_keluarga = 1";
                if($id_jemaat != ''){
                    $where .= " and jemaat.id_jemaat = '".$id_jemaat."'";
                }
                if($kode_jemaat != ''){
                    $where .= " and jemaat.kode_jemaat = '".$kode_jemaat."'";
                }
                if($id_sektor != ''){
                    $where .= " and sektor.id_sektor = '".$id_sektor."'";
                }
                if($id_kelurahan != ''){
                    $where .= " and kelurahan.id_kelurahan = '".$id_kelurahan."'";
                }
                if($id_kecamatan != ''){
                    $where .= " and kecamatan.id_kecamatan = '".$id_kecamatan."'";
                }
                if($id_kabupaten_kota != ''){
                    $where .= " and kabupaten_kota.id_kabupaten_kota = '".$id_kabupaten_kota."'";
                }
                if($id_provinsi != ''){
                    $where .= " and provinsi.id_provinsi = '".$id_provinsi."'";
                }

                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $join[1] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan ', 'direction' => 'left');
                $join[2] = array('tabel' => 'kecamatan', 'relation' => 'kelurahan.id_kecamatan = kecamatan.id_kecamatan', 'direction' => 'left');
                $join[3] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $join[4] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi = provinsi.id_provinsi', 'direction' => 'left');
                $join[5] = array('tabel' => 'hubungan_keluarga', 'relation' => 'hubungan_keluarga.id_hubungan_keluarga = jemaat.id_hubungan_keluarga', 'direction' => 'left');
                $join[6] = array('tabel' => 'jemaat_baptis', 'relation' => 'jemaat_baptis.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[7] = array('tabel' => 'jemaat_kontak', 'relation' => 'jemaat_kontak.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[8] = array('tabel' => 'jemaat_lain_lain', 'relation' => 'jemaat_lain_lain.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[9] = array('tabel' => 'jemaat_pekerjaan', 'relation' => 'jemaat_pekerjaan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[10] = array('tabel' => 'jemaat_pendidikan', 'relation' => 'jemaat_pendidikan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[11] = array('tabel' => 'jemaat_pengalaman_keahlian', 'relation' => 'jemaat_pengalaman_keahlian.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[12] = array('tabel' => 'jemaat_perkawinan', 'relation' => 'jemaat_perkawinan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[13] = array('tabel' => 'jemaat_sidi', 'relation' => 'jemaat_sidi.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[14] = array('tabel' => 'status_perkawinan', 'relation' => 'status_perkawinan.id_status_perkawinan = jemaat_perkawinan.id_status_perkawinan', 'direction' => 'left');
                $join[15] = array('tabel' => 'jenjang_pendidikan', 'relation' => 'jenjang_pendidikan.id_jenjang_pendidikan = jemaat_pendidikan.id_jenjang_pendidikan', 'direction' => 'left');
                $join[16] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');

                $select = "*, jemaat.id_jemaat id_jemaat_asli";
                $data_send = array('where' => $where, 'order' => $order, 'limit' => $limit, 'join' => $join, 'select' => $select);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    foreach ($load_data->result() as $jemaat){
                        unset($jemaat->active);
                        unset($jemaat->user_create);
                        unset($jemaat->user_update);
                        unset($jemaat->time_create);
                        unset($jemaat->time_update);
                        unset($jemaat->id_jemaat);
                        $jemaat->id_jemaat = $jemaat->id_jemaat_asli;
                        unset($jemaat->id_jemaat_asli);

                        $where_anggota = array('jemaat.active' => 1, 'jemaat.id_jemaat_parent' => $jemaat->id_jemaat);
                        $data_send_anggota = array('where' => $where_anggota, 'join' => $join, 'select' => $select);
                        $anggota_jemaat = $this->jemaat->load_data($data_send_anggota);
                        if($anggota_jemaat->num_rows() > 0){
                            foreach ($anggota_jemaat->result() as $anggota){
                                unset($anggota->active);
                                unset($anggota->user_create);
                                unset($anggota->user_update);
                                unset($anggota->time_create);
                                unset($anggota->time_update);
                                unset($anggota->id_jemaat);
                                $anggota->id_jemaat = $anggota->id_jemaat_asli;
                                unset($anggota->id_jemaat_asli);
                            }
                        }
                        $jemaat->anggota_jemaat = $anggota_jemaat->result();
                    }
                }
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }
}
