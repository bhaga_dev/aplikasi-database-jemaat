<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Jenjang_pendidikan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Jenjang_pendidikan_model",'jenjang_pendidikan');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $id_jenjang_pendidikan = htmlentities($this->input->post('id_jenjang_pendidikan'));
        $nama_jenjang_pendidikan = htmlentities($this->input->post('nama_jenjang_pendidikan'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $order = ' id_jenjang_pendidikan ASC';
                $where = "active = 1";
                if($id_jenjang_pendidikan != ''){
                    $where .= " and id_jenjang_pendidikan IN (".$id_jenjang_pendidikan.")";
                }
                if($nama_jenjang_pendidikan != ''){
                    $where .= " and nama_jenjang_pendidikan like '%".$nama_jenjang_pendidikan."%'";
                }

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->jenjang_pendidikan->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = '';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
