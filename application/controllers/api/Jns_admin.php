<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Jns_admin extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Jns_admin_model",'jns_admin');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $page = htmlentities($this->input->post('page'));
        $jml_data = htmlentities($this->input->post('jml_data'));
        $id_jns_admin = htmlentities($this->input->post('id_jns_admin'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $jml_data = ($jml_data ? $jml_data : $this->qty_data_mobile);
                $page = ($page ? $page : 1);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = ' id_jns_admin ASC';
                $where =  array('active' => 1);
                if($id_jns_admin != ''){
                    $where['id_jns_admin'] = $id_jns_admin;
                }

                $data_send = array('where' => $where, 'order' => $order, 'limit' => $limit);
                $load_data = $this->jns_admin->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'token_bermasalah';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = $token_status['message'];
            $return['message'] = 'token_bermasalah';
        }

        echo json_encode($return);
    }

    public function simpan(){
        $token = htmlentities($this->input->post('token'));
        $return = array();
        $token_status = $this->tokenStatusMobile($token, 'SEND_DATA');

        if($token_status['sts']){
            $id_admin = $token_status['data']->profile->id_admin;
            if($this->checkSessionAdmin($token_status['data'])){
                $id_jns_admin = htmlentities($this->input->post('id_jns_admin'));
                $jns_admin = htmlentities($this->input->post('jns_admin'));

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array('jns_admin' => $jns_admin,
                                'user_create' => $id_admin,
                                'time_create' => date('Y-m-d H:i:s'),
                                'time_update' => date('Y-m-d H:i:s'),
                                'user_update' => $id_admin);
                    $exe = $this->jns_admin->save($data, $id_admin, 'mst_admin');
                    if($exe){
                        $return['sts'] = true;
                        $return['data'] = '';
                        $return['message'] = $this->alert('simpan_berhasil', 'mobile');
                    }
                    else{
                        $return['sts'] = false;
                        $return['data'] = '';
                        $return['message'] = $this->alert('proses_gagal', 'mobile');
                    }
                }
                else{
                    $data = array('jns_admin' => $jns_admin,
                                'time_update' => date('Y-m-d H:i:s'),
                                'user_update' => $id_admin);
                    $where = array('id_jns_admin' => $id_jns_admin);
                    $exe = $this->jns_admin->update($data, $where, $id_admin, 'mst_admin');

                    if($exe){
                        $return['sts'] = true;
                        $return['data'] = '';
                        $return['message'] = $this->alert('simpan_berhasil', 'mobile');
                    }
                    else{
                        $return['sts'] = false;
                        $return['data'] = '';
                        $return['message'] = $this->alert('proses_gagal', 'mobile');
                    }
                }
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'token_bermasalah';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = $token_status['message'];
            $return['message'] = 'token_bermasalah';
        }
        echo json_encode($return);
    }
    public function hapus(){
        $token = htmlentities($this->input->post('token'));
        $return = array();
        $token_status = $this->tokenStatusMobile($token, 'SEND_DATA');

        if($token_status['sts']){
            $id_admin = $token_status['data']->profile->id_admin;
            if($this->checkSessionAdmin($token_status['data'])){
                $id_jns_admin = htmlentities($this->input->post('id_jns_admin'));

                $where = array('id_jns_admin' => $id_jns_admin);
                $exe = $this->jns_admin->soft_delete($where, $id_admin, 'mst_admin');

                if($exe){
                    $return['sts'] = true;
                    $return['data'] = '';
                    $return['message'] = $this->alert('hapus_berhasil', 'mobile');
                }
                else{
                    $return['sts'] = false;
                    $return['data'] = '';
                    $return['message'] = $this->alert('proses_gagal', 'mobile');
                }
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'token_bermasalah';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = $token_status['message'];
            $return['message'] = 'token_bermasalah';
        }
        echo json_encode($return);
    }

}
