<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Kelurahan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Kelurahan_model",'kelurahan');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $page = htmlentities($this->input->post('page'));
        $jml_data = htmlentities($this->input->post('jml_data'));
        $id_kelurahan = htmlentities($this->input->post('id_kelurahan'));
        $nama_kecamatan = htmlentities($this->input->post('nama_kelurahan'));

        $id_kecamatan = htmlentities($this->input->post('id_kecamatan'));
        $nama_kelurahan = htmlentities($this->input->post('nama_kecamatan'));

        $id_kabupaten_kota = htmlentities($this->input->post('id_kabupaten_kota'));
        $nama_kabupaten_kota = htmlentities($this->input->post('nama_kabupaten_kota'));

        $id_provinsi = htmlentities($this->input->post('id_provinsi'));
        $nama_provinsi = htmlentities($this->input->post('nama_provinsi'));

        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $jml_data = ($jml_data ? $jml_data : $this->qty_data_mobile);
                $page = ($page ? $page : 1);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $join[0] = array('tabel' => 'kecamatan', 'relation' => 'kecamatan.id_kecamatan = kelurahan.id_kecamatan', 'direction' => 'left');
                $join[1] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $join[2] = array('tabel' => 'provinsi', 'relation' => 'provinsi.id_provinsi = kabupaten_kota.id_provinsi', 'direction' => 'left');
                $order = ' nama_provinsi, nama_kabupaten_kota, nama_kecamatan, nama_kelurahan ASC';

                $where = "kelurahan.active = 1";
                if($id_kelurahan != ''){
                    $where .= " and id_kelurahan = '".$id_kelurahan."'";
                }
                if($nama_kelurahan != ''){
                    $where .= " and nama_kelurahan = like '%".$nama_kelurahan."%'";
                }

                if($id_kecamatan != ''){
                    $where .= " and id_kecamatan = '".$id_kecamatan."'";
                }
                if($nama_kecamatan != ''){
                    $where .= " and nama_kecamatan  like'%".$nama_kecamatan."%'";
                }

                if($id_kabupaten_kota != ''){
                    $where .= " and id_kabupaten_kota= '".$id_kabupaten_kota."'";
                }
                if($nama_kabupaten_kota != ''){
                    $where .= " and nama_kabupaten_kota  like'%".$nama_kabupaten_kota."%'";
                }

                if($id_provinsi != ''){
                    $where .= " and id_provinsi = '".$id_provinsi."'";
                }
                if($nama_provinsi != ''){
                    $where .= " and nama_provinsi  like'%".$nama_provinsi."%'";
                }

                $data_send = array('where' => $where, 'order' => $order, 'limit' => $limit, 'join' => $join);
                $load_data = $this->kelurahan->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = '';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
