<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Pelkat extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Pelkat_model",'pelkat');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));

        $id_pelkat = htmlentities($this->input->post('id_pelkat'));
        $nama_pelkat = htmlentities($this->input->post('nama_pelkat'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $order = ' id_pelkat ASC';
                $where = "active = 1";
                if($id_pelkat != ''){
                    $where .= " and id_pelkat IN (".$id_pelkat.")";
                }
                if($nama_pelkat != ''){
                    $where .= " and nama_pelkat like '%".$nama_pelkat."%'";
                }

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->pelkat->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
