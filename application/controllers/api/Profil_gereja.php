<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Profil_gereja extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Profil_gereja_model",'profil_gereja');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){

                $order = ' id_profil_gereja ASC';
                $where =  array('active' => 1, 'id_profil_gereja' => 1);

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->profil_gereja->load_data($data_send);
                $result = $load_data->row();

                if($result->logo_gereja){
                    $result->logo_gereja = base_url().$result->logo_gereja;
                }

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
