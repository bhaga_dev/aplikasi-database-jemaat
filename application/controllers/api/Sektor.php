<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Sektor extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Sektor_model",'sektor');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $id_sektor = htmlentities($this->input->post('id_sektor'));
        $nama_sektor = htmlentities($this->input->post('nama_sektor'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $order = ' id_sektor ASC';

                $where = "active = 1";
                if($id_sektor != ''){
                    $where .= " and id_sektor IN (".$id_sektor.")";
                }
                if($nama_sektor != ''){
                    $where .= " and nama_sektor like '%".$nama_sektor."%'";
                }

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->sektor->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }
}
