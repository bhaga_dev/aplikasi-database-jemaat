<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Status_perkawinan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Status_perkawinan_model",'status_perkawinan');
	}

    public function load_data(){
        $token = htmlentities($this->input->post('token'));
        $id_status_perkawinan = htmlentities($this->input->post('id_status_perkawinan'));
        $nama_status_perkawinan = htmlentities($this->input->post('nama_status_perkawinan'));
        $token_status = $this->tokenStatusMobile($token, 'LOAD_DATA');
        if($token_status['sts']){
            if($this->checkSessionAdmin($token_status['data'])){
                $order = ' id_status_perkawinan ASC';
                $where = "active = 1";
                if($id_status_perkawinan != ''){
                    $where .= " and id_status_perkawinan IN (".$id_status_perkawinan.")";
                }
                if($nama_status_perkawinan != ''){
                    $where .= " and nama_status_perkawinan like '%".$nama_status_perkawinan."%'";
                }

                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->status_perkawinan->load_data($data_send);
                $result = $load_data->result();

                $return['sts'] = true;
                $return['data'] = $result;
                $return['message'] = '';
            }
            else{
                $return['sts'] = false;
                $return['data'] = 'kadaluarsa';
                $return['message'] = 'Token kadaluarsa';
            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'Token bermasalah';
        }

        echo json_encode($return);
    }

}
