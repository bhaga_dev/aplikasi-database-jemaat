
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_pelkat extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
        $this->load->model("pelkat_model","pelkat");
    }

    public function index(){
        $id_sektor = htmlentities($this->input->get('sektor'));
        $id_pelkat = htmlentities($this->input->get('pelkat'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $where = array('active' => 1, 'id_pelkat' => $id_pelkat);
                $data_send = array('where' => $where);
                $pelkat_load = $this->pelkat->load_data($data_send);
                if($pelkat_load->num_rows() > 0){
                    $pelkat = $pelkat_load->row();

                    $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                    $join[1] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan', 'direction' => 'left');
                    $order = "kode_sektor ASC";

                    $where = "jemaat.active = 1 and jemaat.id_pelkat = '".$id_pelkat."'";
                    if($id_sektor and $id_sektor != 'all')
                        $where .= " and jemaat.id_sektor = '".$id_sektor."'";

                    $data_send = array('where' => $where, 'order' => $order, 'join' => $join);
                    $load_data = $this->jemaat->load_data($data_send);

                    $nama_sektor = 'Semua '.ucwords($this->lang('sektor'));
                    if($id_sektor and $id_sektor != 'all'){
                        $this->load->model("sektor_model", "sektor");
                        $where_sektor = array('active' => 1, 'id_sektor' => $id_sektor);
                        $data_send_sektor = array('where' => $where_sektor);
                        $load_data_sektor = $this->sektor->load_data($data_send_sektor);
                        if($load_data_sektor->num_rows() > 0){
                            $nama_sektor = $load_data_sektor->row()->nama_sektor;
                        }
                    }

                    $konten = array('data' =>$load_data, 'pelkat' => $pelkat, 'sektor' => $nama_sektor);
                    $this->load->view('laporan/anggota_pelkat_print', $this->data_halaman($konten));
                }
            }
        }
    }
}
