
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kartu_jemaat extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
        $this->load->model("profil_gereja_model","profil_gereja");
    }

    public function index(){
        $kode_kk = htmlentities($this->input->get('kode_kk'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $kode_kk){
                $where = array('active' => 1, 'id_profil_gereja' => '1');
                $data_send = array('where' => $where);
                $profil_gereja = $this->profil_gereja->load_data($data_send)->row();

                $order = "jemaat.id_hubungan_keluarga ASC";
                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $where = "jemaat.active = 1 and (jemaat.id_jemaat = '".$kode_kk."' or jemaat.id_jemaat_parent = '".$kode_kk."')";
                $data_send = array('where' => $where, 'order' => $order, 'join' => $join);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $jemaat = $load_data->result();

                    $konten = array('jemaat' =>$jemaat, 'kode_kk' => $kode_kk, 'profil_gereja' => $profil_gereja);
                    $this->load->view('jemaat/kartu_jemaat_print', $this->data_halaman($konten));
                }
                else{
                    $this->load->view('jemaat/jemaat_tidak_ada', $this->data_halaman());
                }
            }
        }
    }

}
