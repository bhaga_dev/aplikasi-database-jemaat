
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kwj extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
        $this->load->model("profil_gereja_model","profil_gereja");
    }

    public function index(){
        $kode_kk = htmlentities($this->input->get('kode_kk'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $kode_kk){
                $where = array('active' => 1, 'id_profil_gereja' => '1');
                $data_send = array('where' => $where);
                $profil_gereja = $this->profil_gereja->load_data($data_send)->row();

                $order = "jemaat.id_hubungan_keluarga ASC";
                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $join[1] = array('tabel' => 'kelurahan', 'relation' => 'kelurahan.id_kelurahan = jemaat.id_kelurahan ', 'direction' => 'left');
                $join[2] = array('tabel' => 'kecamatan', 'relation' => 'kelurahan.id_kecamatan = kecamatan.id_kecamatan', 'direction' => 'left');
                $join[3] = array('tabel' => 'kabupaten_kota', 'relation' => 'kabupaten_kota.id_kabupaten_kota = kecamatan.id_kabupaten_kota', 'direction' => 'left');
                $join[4] = array('tabel' => 'provinsi', 'relation' => 'kabupaten_kota.id_provinsi = provinsi.id_provinsi', 'direction' => 'left');
                $join[5] = array('tabel' => 'hubungan_keluarga', 'relation' => 'hubungan_keluarga.id_hubungan_keluarga = jemaat.id_hubungan_keluarga', 'direction' => 'left');
                $join[6] = array('tabel' => 'jemaat_baptis', 'relation' => 'jemaat_baptis.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[7] = array('tabel' => 'jemaat_kontak', 'relation' => 'jemaat_kontak.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[8] = array('tabel' => 'jemaat_lain_lain', 'relation' => 'jemaat_lain_lain.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[9] = array('tabel' => 'jemaat_pekerjaan', 'relation' => 'jemaat_pekerjaan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[10] = array('tabel' => 'jemaat_pendidikan', 'relation' => 'jemaat_pendidikan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[11] = array('tabel' => 'jemaat_pengalaman_keahlian', 'relation' => 'jemaat_pengalaman_keahlian.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[12] = array('tabel' => 'jemaat_perkawinan', 'relation' => 'jemaat_perkawinan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[13] = array('tabel' => 'jemaat_sidi', 'relation' => 'jemaat_sidi.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $join[14] = array('tabel' => 'status_perkawinan', 'relation' => 'status_perkawinan.id_status_perkawinan = jemaat_perkawinan.id_status_perkawinan', 'direction' => 'left');
                $join[15] = array('tabel' => 'jenjang_pendidikan', 'relation' => 'jenjang_pendidikan.id_jenjang_pendidikan = jemaat_pendidikan.id_jenjang_pendidikan', 'direction' => 'left');
                $join[16] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');
                $where = "jemaat.active = 1 and (jemaat.id_jemaat_parent = '".$kode_kk."%' or jemaat.id_jemaat = '".$kode_kk."')";
                $data_send = array('where' => $where, 'order' => $order, 'join' => $join);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $jemaat = $load_data->result();

                    $str = "select min(id_jemaat) id_jemaat from jemaat where id_jemaat > '".$kode_kk."' and id_jemaat_parent = 0";
                    $next_exe = $this->jemaat->query($str);
                    if($next_exe->num_rows() > 0)
                        $next = $next_exe->row()->id_jemaat;
                    else
                        $next = null;

                    $str = "select max(id_jemaat) id_jemaat from jemaat where id_jemaat < '".$kode_kk."' and id_jemaat_parent = 0";
                    $prev_exe = $this->jemaat->query($str);
                    if($prev_exe->num_rows() > 0)
                        $prev = $prev_exe->row()->id_jemaat;
                    else
                        $prev = null;

                    $konten = array('jemaat' => $jemaat,
                        'kode_kk' => $kode_kk,
                        'profil_gereja' => $profil_gereja,
                        'next' => ($next ? base_url().'laporan/kwj/?kode_kk='.$next."&token=".$this->getToken('LOAD_DATA') : '#'),
                        'prev' => ($prev ? base_url().'laporan/kwj/?kode_kk='.$prev."&token=".$this->getToken('LOAD_DATA') : '#'),
                    );
                    $this->load->view('jemaat/kwj_print', $this->data_halaman($konten));
                }
                else{
                    $this->load->view('jemaat/jemaat_tidak_ada', $this->data_halaman());
                }
            }
        }
    }

}
