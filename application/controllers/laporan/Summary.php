
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
    }

    public function jemaat_pelkat(){
        $data_receive = json_decode(urldecode($this->input->post('data_send')));
        $token = $data_receive->token;

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $select = "singkatan_pelkat label, count(-1) value, nama_pelkat";
                $where = "jemaat.active = 1 and pelkat.active = 1";
                $group = "jemaat.id_pelkat";
                $join[0] = array('tabel' => 'pelkat', 'relation' => 'pelkat.id_pelkat = jemaat.id_pelkat', 'direction' => 'left');
                $data_send = array('select' => $select, 'where' => $where, 'join' => $join, 'group' => $group);
                $result = $this->jemaat->load_data($data_send)->result();

                echo json_encode($result);
            }
        }
    }
    public function kk_sektor(){
        $data_receive = json_decode(urldecode($this->input->post('data_send')));
        $token = $data_receive->token;

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $select = "nama_sektor label, count(-1) value";
                $where = "jemaat.active = 1 and sektor.active = 1 and jemaat.id_hubungan_keluarga = 1";
                $group = "jemaat.id_sektor";
                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $data_send = array('select' => $select, 'where' => $where, 'join' => $join, 'group' => $group);
                $result = $this->jemaat->load_data($data_send)->result();

                echo json_encode($result);
            }
        }
    }
    public function anggota_keluarga_sektor(){
        $data_receive = json_decode(urldecode($this->input->post('data_send')));
        $token = $data_receive->token;

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $select = "nama_sektor label, count(-1) value";
                $where = "jemaat.active = 1 and sektor.active = 1";
                $group = "jemaat.id_sektor";
                $join[0] = array('tabel' => 'sektor', 'relation' => 'sektor.id_sektor = jemaat.id_sektor', 'direction' => 'left');
                $data_send = array('select' => $select, 'where' => $where, 'join' => $join, 'group' => $group);
                $result = $this->jemaat->load_data($data_send)->result();

                echo json_encode($result);
            }
        }
    }
    public function jns_kelamin(){
        $data_receive = json_decode(urldecode($this->input->post('data_send')));
        $token = $data_receive->token;

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $select = "IF(jenis_kelamin = 'L', 'Laki-Laki', IF(jenis_kelamin = 'P', 'Perempuan', 'Tidak Diketahui')) label, count(-1) value";
                $where = "jemaat.active = 1";
                $group = "jemaat.jenis_kelamin";

                $data_send = array('select' => $select, 'where' => $where, 'group' => $group);
                $result = $this->jemaat->load_data($data_send)->result();

                echo json_encode($result);
            }
        }
    }
    public function jemaat_berdasarkan_usia(){
        $data_receive = json_decode(urldecode($this->input->post('data_send')));
        $token = $data_receive->token;

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                /*
                 * 0 - 1 = Bayi
                 * 2 - 10 = Anak-anak
                 * 11 - 19 = Remaja
                 * 20 - 60 = Dewasa
                 * 61 keatas = Lanjut usia
                 *
                 * Menurut WHO
                 * */
                $str = "SELECT 
                            CASE
                            WHEN umur <= 1 THEN 'Bayi'
                            WHEN umur BETWEEN 2 AND 10 THEN 'Anak-Anak'
                            WHEN umur BETWEEN 11 AND 19 THEN 'Remaja'
                            WHEN umur BETWEEN 20 AND 60 THEN 'Dewasa'
                            WHEN umur > 61 THEN 'Lanjut Usia'
                            ELSE 'Tidak Diketahui'
                            END AS label,
                            COUNT(*) AS `value`,
                             CASE
                            WHEN umur <= 1 THEN 1
                            WHEN umur BETWEEN 2 AND 10 THEN 2
                            WHEN umur BETWEEN 11 AND 19 THEN 3
                            WHEN umur BETWEEN 20 AND 60 THEN 4
                            WHEN umur > 61 THEN 5
                            ELSE 6
                            END AS ordinal
                         FROM (
                        SELECT tgl_lahir, IFNULL(TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()), 0) umur
                        FROM jemaat) usia_jemaat
                        GROUP BY label
                        ORDER BY ordinal ASC";
                $load_data = $this->jemaat->query($str);
                $result = $load_data->result();

                echo json_encode($result);
            }
        }
    }

}
