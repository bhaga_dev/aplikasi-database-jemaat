
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ultah_kawin extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
    }

    public function index(){
        $periode_awal = htmlentities($this->input->get('periode_awal'));
        $periode_akhir = htmlentities($this->input->get('periode_akhir'));
        $field = htmlentities($this->input->get('field'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir){
                $field = ($field ? $field : 'tgl_kawin_gereja');
                $periode_awal_format = substr($this->reformat_date($periode_awal, '-'), 5);
                $periode_akhir_format = substr($this->reformat_date($periode_akhir, '-'), 5);
                $join[0] = array('tabel' => 'jemaat_perkawinan', 'relation' => 'jemaat_perkawinan.id_jemaat = jemaat.id_jemaat', 'direction' => 'left');
                $order = "UNIX_TIMESTAMP(date_format(".$field.", '%m-%d')) ASC";
                $select = "*, ".$field." as tgl_kawin";
                #id_hubungan_keluarga harus 1 karena laporan ini hanya menampilkan ultah perkawinan milik kepala keluarga...
                $where = "jemaat.active = 1 and jemaat.id_hubungan_keluarga = 1 and date_format(".$field.", '%m-%d') between '".$periode_awal_format."' and '".$periode_akhir_format."'";

                $data_send = array('where' => $where, 'order' => $order, 'select' => $select, 'join' => $join);
                $load_data = $this->jemaat->load_data($data_send);
                if($load_data->num_rows() > 0){
                    foreach ($load_data->result() as $row){
                        $nama_istri = '';
                        $where_istri = "active = 1 and id_hubungan_keluarga = 2 and id_jemaat_parent = '".$row->id_jemaat."'";
                        $data_send_istri = array('where' => $where_istri);
                        $load_data_istri = $this->jemaat->load_data($data_send_istri);
                        if($load_data_istri->num_rows() > 0){
                            $nama_istri = $load_data_istri->row()->nama_jemaat;
                        }
                        $row->istri = $nama_istri;
                    }
                }

                $tgl_pecah = explode('-', $periode_awal);
                $periode_awal = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];
                $tgl_pecah = explode('-', $periode_akhir);
                $periode_akhir = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];

                $konten = array('data' =>$load_data, 'periode_awal' => $periode_awal, 'periode_akhir' => $periode_akhir);
                $this->load->view('laporan/ultah_kawin_print', $this->data_halaman($konten));
            }
        }
    }

}
