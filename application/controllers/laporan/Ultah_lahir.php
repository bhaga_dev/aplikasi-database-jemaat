
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ultah_lahir extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("jemaat_model","jemaat");
    }

    public function index(){
        $periode_awal = htmlentities($this->input->get('periode_awal'));
        $periode_akhir = htmlentities($this->input->get('periode_akhir'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir){
                $periode_awal_format = substr($this->reformat_date($periode_awal, '-'), 5);
                $periode_akhir_format = substr($this->reformat_date($periode_akhir, '-'), 5);
                $order = "UNIX_TIMESTAMP(date_format(tgl_lahir, '%m-%d')) ASC";
                $where = "jemaat.active = 1 and date_format(tgl_lahir, '%m-%d') between '".$periode_awal_format."' and '".$periode_akhir_format."'";
                $data_send = array('where' => $where, 'order' => $order);
                $load_data = $this->jemaat->load_data($data_send);

                $tgl_pecah = explode('-', $periode_awal);
                $periode_awal = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];
                $tgl_pecah = explode('-', $periode_akhir);
                $periode_akhir = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];

                $konten = array('data' =>$load_data, 'periode_awal' => $periode_awal, 'periode_akhir' => $periode_akhir);
                $this->load->view('laporan/ultah_lahir_print', $this->data_halaman($konten));
            }
        }
    }

}
