<?php
//require_once APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . '/libraries/JWT.php';
require_once APPPATH . '/libraries/ExpiredException.php';
require_once APPPATH . '/libraries/SignatureInvalidException.php';
use \Firebase\JWT\JWT;

class MY_Controller extends CI_Controller {
    var $versi = '1.1.0';
    var $nama_developer = 'Bhaga ISD';
    var $url_developer = 'http://www.bhaga.id/';

    var $aplikasi = 'Aplikasi Data Jemaat';
    var $nama_instansi = '';
    var $alamat = '';
    var $telepon = '';
    var $email = '';
    var $metatag = '';
    var $deskripsi_web = '';
    var $no_photo = 'assets/images/no_image.jpg';
    var $no_avatar = 'assets/images/avatar.jpg';
    var $kolom_label = 'col-sm-3';
    var $red_star = '<span class="fa fa-star" style="color:red;font-size: 10px;position: absolute;margin-left: 5px;"></span>';
    var $mata_uang = 'Rp';
    var $maxlength_id = 5;
    var $qty_data = 10;
    var $qty_data_mobile = 8;
    var $waiting_time = 1500; #1.5 second

    //JSON web token..
    //don't change secretKey and tokenKey unless needed..
    private $jwt = array('secretKey' => 'GLCMVMLLMURYCOC4T',
                        'staticKey' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9',
                        'algorithm' => 'HS256');

    //social media link..

    function __construct()
    {
        parent::__construct();
    }

    public function getToken($type_token = 'LOAD_DATA'){
        //persamaan: 1 menit = 600;
        if($type_token == 'SEND_DATA')
            $expiredIn = 72000; //2 jam...
        else
            $expiredIn = 18000; // 30 menit...
        $date = new DateTime();
        $token['type']  = $type_token; //REQUEST_DATA & ENTRY_DATA..
        $token['time']  = $date->getTimestamp();
        $token['expd']  = $date->getTimestamp() + $expiredIn;
        $output = JWT::encode($token, $this->jwt['secretKey'], $this->jwt['algorithm']);

        $enkrip = explode('.', $output);
        $output = $enkrip[1].'.'.$enkrip[2];
        return $output;
    }
    public function tokenStatus($token, $type_token){
        try{
            $date = new DateTime();
            $decode = JWT::decode($this->jwt['staticKey'].'.'.$token, $this->jwt['secretKey'], array($this->jwt['algorithm']));

            if($type_token == $decode->type){
                if($date->getTimestamp() < $decode->expd)
                    return true;
                else
                    return false;
            }
            else
                return false;

        }catch(Exception $e) {
            $msg = $e->getMessage(); //this is the way to get error message from jwt.. but I use only false..
            return false;
        }

    }
    public function getTokenMobile($receiver_data, $type_token = 'LOAD_DATA', $login_status = false){
        $date = new DateTime();
        $now = $date->getTimestamp();
        $expd_date = strtotime('+3 day', $now);
        $token['type']  = $type_token; //LOAD_DATA & SEND_DATA..
        $token['time']  = $now;
        $token['expd']  = $expd_date;
        $token['profile']  = $receiver_data;
        $token['login_status']  = $login_status; //(isset($receiver_data->login_status) ? $receiver_data->login_status : 'false');

        $output = JWT::encode($token, $this->jwt['secretKey'], $this->jwt['algorithm']);

        $enkrip = explode('.', $output);
        $output = $enkrip[1].'.'.$enkrip[2];
        return $output;
    }
    public function tokenStatusMobile($token, $type_token, $check_user_data = true){
        $return = array();
        try{
            $date = new DateTime();
            $decode = JWT::decode($this->jwt['staticKey'].'.'.$token, $this->jwt['secretKey'], array($this->jwt['algorithm']));

            $status = false;
            if($type_token == $decode->type){
                if($date->getTimestamp() < $decode->expd){
                    if($check_user_data == true){
                        $return['sts'] = true;
                        $return['data'] = $decode;
                        $return['message'] = 'success_check';
                    }
                    else{
                        $return['sts'] = true;
                        $return['data'] = '';
                        $return['message'] = 'success_check';
                    }
                }
                else{
                    $return['sts'] = false;
                    $return['data'] = '';
                    $return['message'] = 'kadaluarsa';
                }
            }
            else{
                $return['sts'] = false;
                $return['data'] = '';
                $return['message'] = 'tipe_token_salah';
            }
        }catch(Exception $e) {
            $msg = $e->getMessage(); //this is the way to get error message from jwt.. but I use only false..
            $return['sts'] = false;
            $return['data'] = '';
            $return['message'] = 'token_invalid';
        }

        return $return;
    }
    public function checkSessionAdmin($data){
        $username = $data->profile->username_admin;
        $password = $data->profile->password_admin;

        $this->load->model('Mst_admin_model', 'admin');
        $result = $this->admin->checkAdmin($username, $password, 'mobile');
        if($result)
            return true;
        else
            return false;
    }

    function data_halaman($konten=''){
        $data_halaman = array(
            "versi"                => $this->versi,
            "konten"                => $konten,
            "url_developer"         => $this->url_developer,
            "nama_developer"        => $this->nama_developer,
            "aplikasi" 	            => $this->aplikasi,
            "title" 		        => $this->aplikasi.($this->nama_instansi ? ' - '.$this->nama_instansi : ''),
            "nama_instansi"         => $this->nama_instansi,
            "deskripsi_web"         => $this->deskripsi_web,
            "metatag" 	            => $this->metatag,
            "kolom_label" 	        => $this->kolom_label,
            "red_star" 		        => $this->red_star,
            "maxlength_id"          => $this->maxlength_id,
            "waiting_time"          => $this->waiting_time,
            "qty_data"              => $this->qty_data,
            "no_photo"              => 'onerror="this.onerror=null;this.src=\''.base_url().$this->no_photo.'\';"',
            "no_photo_url"          => $this->no_photo,
            "no_photo_for_js"       => 'onerror="this.onerror=null;this.src=\\\''.base_url().$this->no_photo.'\\\';"',
            "no_avatar"             => 'onerror="this.onerror=null;this.src=\''.base_url().$this->no_avatar.'\';"',
            "no_avatar_for_js"      => 'onerror="this.onerror=null;this.src=\\\''.base_url().$this->no_avatar.'\\\';"',
            "no_avatar_url"         => $this->no_avatar,
            "today" 	            => $day = $this->hari(date('w')).', '.date('d').' '.$this->bulan((int)date('m')).' '.date('Y')
        );

        return $data_halaman;
    }
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function reformat_date($date, $delimiter, $time = false){
        if($date){
            $waktu = '';
            if($time){
                $pecah = explode(" ", $date);
                $date = $pecah[0];
                $waktu = ' '.$pecah[1];
            }
            $date_split = explode($delimiter, $date);

            $new_date = $date_split[2].'-'.$date_split[1].'-'.$date_split[0].$waktu;
            return $new_date;
        }
        else
            return null;
    }
    function lost(){
        //halaman tidak ditemukan...
        $this->load->view($this->dir_view.'lost', $this->data_halaman());

    }
    function hari($line)
    {
        $hari[0] = 'Minggu';
        $hari[1] = 'Senin';
        $hari[2] = 'Selasa';
        $hari[3] = 'Rabu';
        $hari[4] = 'Kamis';
        $hari[5] = 'Jumat';
        $hari[6] = 'Sabtu';

        return $hari[$line];
    }
    function bulan($i){
        $bulan[1] = 'Januari';
        $bulan[2] = 'Februari';
        $bulan[3] = 'Maret';
        $bulan[4] = 'April';
        $bulan[5] = 'Mei';
        $bulan[6] = 'Juni';
        $bulan[7] = 'Juli';
        $bulan[8] = 'Agustus';
        $bulan[9] = 'September';
        $bulan[10] = 'Oktober';
        $bulan[11] = 'November';
        $bulan[12] = 'Desember';

        return $bulan[$i];
    }
    function alert_text($i){
        $alert['simpan_berhasil']['sts'] = "Proses simpan data berhasil dilakukan.";
        $alert['simpan_berhasil']['type'] = "success";
        $alert['hapus_berhasil']['sts']  = "Proses hapus data berhasil dilakukan.";
        $alert['hapus_berhasil']['type'] = "success";
        $alert['reset_password_berhasil']['sts']  = "Proses reset sandi berhasil dilakukan.";
        $alert['reset_password_berhasil']['type'] = "success";
        $alert['ganti_pwd_berhasil']['sts']  = "Proses ganti sandi berhasil dilakukan.";
        $alert['ganti_pwd_berhasil']['type'] = "success";
        $alert['login_berhasil']['sts']  = "Login berhasil, silahkan tunggu sebentar.";
        $alert['login_berhasil']['type'] = "success";

        $alert['semua_wajib']['sts']  = "Semua field wajib dilengkapi.";
        $alert['semua_wajib']['type'] = "warning";
        $alert['kosong']['sts']  = "Field yang memiliki tanda bintang wajib dilengkapi.";
        $alert['kosong']['type'] = "warning";
        $alert['email_salah']['sts']  = "Format email yang anda masukkan salah.";
        $alert['email_salah']['type'] = "warning";
        $alert['password_tidak_sama']['sts']  = "Sandi yang anda masukan tidak sama dengan yang anda ulangi.";
        $alert['password_tidak_sama']['type'] = "warning";
        $alert['password_salah']['sts']  = "Sandi lama yang anda masukan salah.";
        $alert['password_salah']['type'] = "warning";
        $alert['email_available']['sts']  = "Email yang anda masukan sudah terdaftar, silahkan gunakan email lain.";
        $alert['email_available']['type'] = "warning";
        $alert['username_available']['sts']  = "Username yang anda masukan sudah terdaftar, silahkan gunakan username lain.";
        $alert['username_available']['type'] = "warning";
        $alert['not_valid']['sts']  = "Kombinasi username dan sandi tidak sesuai, silahkan ulangi lagi.";
        $alert['not_valid']['type'] = "warning";
        $alert['presensi_tutup']['sts']  = "Waktu presensi kegiatan sudah ditutup.";
        $alert['presensi_tutup']['type'] = "warning";
        $alert['format_waktu_salah']['sts']  = "Waktu yang anda masukkan salah. Aplikasi hanya menerima format dd-mm-yyyy HH:MM. Contoh: 17-01-2017 17:58.";
        $alert['format_waktu_salah']['type'] = "warning";
        $alert['format_tgl_salah']['sts']  = "Tanggal yang anda masukkan salah. Aplikasi hanya menerima format dd-mm-yyyy. Contoh: 17-01-2017.";
        $alert['format_tgl_salah']['type'] = "warning";
        $alert['format_jam_salah']['sts']  = "Jam yang anda masukkan salah. Aplikasi hanya menerima format HH:MM. Contoh: 17:56.";
        $alert['format_jam_salah']['type'] = "warning";
        $alert['id_kembar']['sts']  = "Anda tidak bisa menggunakan ID tersebut karena sudah terdaftar didalam database.";
        $alert['id_kembar']['type'] = "warning";;
        $alert['upload_error']['sts']  = "Upload error: <span id=\"upload_error_msg\"></span>";
        $alert['upload_error']['type'] = "warning";

        $alert['proses_gagal']['sts']  = "Ada kesalahan dalam proses, silahkan ulangi sekali lagi.";
        $alert['proses_gagal']['type'] = "error";
        $alert['tidak_berhak_ubah_data']['sts']  = "Anda tidak berhak untuk mengubah data ini.";
        $alert['tidak_berhak_ubah_data']['type'] = "error";
        $alert['tidak_berhak_hapus_data']['sts']  = "Anda tidak berhak untuk menghapus data ini.";
        $alert['tidak_berhak_hapus_data']['type'] = "error";
        $alert['token_invalid']['sts']  = "Token invalid, silahkan buka ulang (refresh) halaman ini.";
        $alert['token_invalid']['type'] = "error";
        $alert['token_kadaluarsa']['sts']  = "Token kadaluarsa, silahkan login ulang.";
        $alert['token_kadaluarsa']['type'] = "error";

        $x['msg'] = $alert[$i]['sts'];
        $x['type'] = $alert[$i]['type'];
        return $x;
    }
    function alert($i, $for = ''){
        $alert = $this->alert_text($i);
        $info = '';
        if($alert['type'] == 'success')
            $info = 'Sukses!';
        else if($alert['type'] == 'warning')
            $info = 'Perhatian!';
        else if($alert['type'] == 'error')
            $info = 'Maaf!';

        if($for == 'mobile')
            return $alert['msg'];
        else
            return 'toastr.'.$alert['type'].'("'.$alert['msg'].'", "'.$info.'");';

    }

    function redirect($url){
        echo "<script>
			location.href='$url';
		</script>";
    }

    public function validasi_controller($menu){
        $return = false;

        $this->load->model('Hak_akses_menu_model', 'hak_akses_menu');
        $where = array("id_jns_admin = (SELECT a.id_jns_admin FROM mst_admin a WHERE a.id_admin = '".$this->session->userdata('id_admin')."')" => null,
            "id_menu = (SELECT b.id_menu FROM menu b WHERE b.url_menu = '".$menu."')" => null);
        $exe = $this->hak_akses_menu->cek_duplikat($where);

        if($exe == 'available')
            $return = true;
        else
            $return = false;

        if($return === false){
            $this->redirect(base_url().'gateway/keluar');
        }
        return $return;

    }
    public function validasi_login(){
        if($this->session->userdata('login_as') == 'administrator' and $this->session->userdata('id_admin') != ''){
            //validasi id user and password..
            $username_admin = $this->session->userdata('username_admin');
            $password_admin = $this->session->userdata('password_admin');

            $this->load->model('Mst_admin_model', 'admin');
            $where = array('username_admin' => $username_admin, 'password_admin' => $password_admin);
            $exe = $this->admin->cek_duplikat($where);

            if($exe == 'available')
                return true;
            else
                return false;

        }
        else
            return false;
    }
    public function http_request_builder($data, $url){

        $protocol = substr($url, 0, 5);
        if($protocol == 'https'){

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POST, 1);
// Edit: prior variable $postFields should be $postfields;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
            $result = curl_exec($ch);

		}
		else{
			$postdata = http_build_query($data);
			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata
				)
			);

			$context = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);

		}

        return $result;
    }
    function upload($variable = array()){
        $return = array();
        $this->load->helper('file');
        $new_name = $this->session->userdata('id_admin').'_'.date('ymdHis').'_'.$this->generateRandomString(5);
        $file = $variable['file'];
        $dir = $variable['dir'];
        if(isset($variable['overwrite']))
            $overwrite = $variable['overwrite'];
        else
            $overwrite = false;
        if(isset($variable['manimpulation']))
            $manimpulation = $variable['manimpulation'];
        else
            $manimpulation = false;

        if(isset($_FILES[$file]))
        {
            $sts_watermark = true;
            $nama = $_FILES[$file]['name'];
            $manual_mime = get_mime_by_extension($nama);
            if(isset($this->manual_tipe_file)){
                if($manual_mime == $this->manual_tipe_file)
                    $way_through = true;
                else
                    $way_through = false;
            }
            else
                $way_through = true;

            if($way_through){
                $config['upload_path'] = $dir;
                $config['allowed_types'] = $this->tipe_file;
                $config['file_name'] = strtolower($nama);
                $config['overwrite'] = $overwrite;
                $config['file_name'] = $new_name.'.'.pathinfo($nama, PATHINFO_EXTENSION);
//            $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                if($this->upload->do_upload($file))
                {
                    $image_data = $this->upload->data();

                    if($manimpulation){
                        ini_set('memory_limit', '-1');
                        //proses manipulasi gambar asli..
                        $img_proses['new_image']    = $dir."/".$image_data['file_name'];
                        $img_proses['width']	    = 1000;
                        $img_proses['source_image'] = $dir."/".$image_data['file_name'];
                        $img_proses['quality']      = 70; //compress the image..

                        $this->load->library('image_lib', $img_proses);
                        $this->image_lib->initialize($img_proses);
                        $this->image_lib->resize();
                        $this->image_lib->watermark();


                    }

                    $return['sts'] = 'sukses';
                    $return['msg'] = '';
                    $return['file'] = $image_data['file_name'];

                }
                else{
                    $return['sts'] = 'denied';
                    $return['msg'] = $this->upload->display_errors('', '');
                }
            }
            else{
                $return['sts'] = 'denied';
                $return['msg'] = '';
            }
        }
        else{
            $return['sts'] = 'form_empty';
            $return['msg'] = '';
        }

        return $return;
    }

    function base64_to_file($base64_string, $output_file, $filename) {
        $data = explode( ',', $base64_string );
        $img = $data[1];
        $data = base64_decode($img);
        $file = $output_file .$filename;
        $success = file_put_contents($file, $data);

        return $success ? $file : '';
    }
    function create_kode_jemaat($id_sektor, $id_hubungan_keluarga, $id_jemaat_parent, $kepala_keluarga_saja = false){
        $this->load->model("jemaat_model", "jemaat");
        $this->load->model("profil_gereja_model", "profil_gereja");
        $this->load->model("sektor_model", "sektor");
        $this->load->model("hubungan_keluarga_model", "hubungan_keluarga");
        $kode_jemaat = '';
        if($id_jemaat_parent != 0){
            $where_jemaat = array('active' => 1, 'id_jemaat' => $id_jemaat_parent);
            $data_send_jemaat = array('where' => $where_jemaat);
            $load_data_jemaat = $this->jemaat->load_data($data_send_jemaat);
            if($load_data_jemaat->num_rows() > 0){
                $kode_jemaat = substr($load_data_jemaat->row()->kode_jemaat,0, -2);
            }
        }
        else{
            $where = array('active' => 1);
            $data_send = array('where' => $where);
            $load_data = $this->profil_gereja->load_data($data_send);
            if($load_data->num_rows() > 0){
                $gereja = $load_data->row();
                $kode_jemaat = ($gereja->kode_gereja ? $gereja->kode_gereja.'-' : '');

                #mencari kode sektor
                $where_sektor = array('active' => 1, 'id_sektor' => $id_sektor);
                $data_send_sektor = array('where' => $where_sektor);
                $load_data_sektor = $this->sektor->load_data($data_send_sektor);
                if($load_data_sektor->num_rows() > 0){
                    $sektor = $load_data_sektor->row();

                    $kode_jemaat .= ($sektor->kode_sektor ? $sektor->kode_sektor.'-' : '');
                }

                #mencari nomor_urut...
                $jml_start = strlen($kode_jemaat) + 1;
                $select_sektor = "(IFNULL(MAX(CAST(SUBSTRING(kode_jemaat, ".$jml_start.", 3) AS UNSIGNED)), 0) + 1) nomor_urut";
                $where_sektor = array('active' => 1, 'id_sektor' => $id_sektor);
                if($kepala_keluarga_saja){
                    $where_sektor['id_hubungan_keluarga'] = 1;
                }
                $data_send_sektor = array('where' => $where_sektor, 'select' => $select_sektor);
                $load_data_sektor = $this->jemaat->load_data($data_send_sektor);
                if($load_data_sektor->num_rows() > 0){
                    $nomor_urut = $load_data_sektor->row()->nomor_urut;

                    if($nomor_urut < 10)
                        $nomor_urut = '00'.$nomor_urut;
                    else if($nomor_urut < 100)
                        $nomor_urut = '0'.$nomor_urut;


                    $kode_jemaat .= $nomor_urut.'-';
                }
            }
        }

        #mencari hubungan keluarga
        $where_hubungan_keluarga = array('active' => 1, 'id_hubungan_keluarga' => $id_hubungan_keluarga);
        $data_send_hubungan_keluarga = array('where' => $where_hubungan_keluarga);
        $load_data_hubungan_keluarga = $this->hubungan_keluarga->load_data($data_send_hubungan_keluarga);
        if($load_data_hubungan_keluarga->num_rows() > 0){
            $hubungan_keluarga = $load_data_hubungan_keluarga->row();

            $kode_jemaat .= $hubungan_keluarga->kode_hubungan_keluarga;
            if($hubungan_keluarga->kode_hubungan_keluarga != '01' and $hubungan_keluarga->kode_hubungan_keluarga != '02'){
                #melakukan perhitungan kode...
                $jml_start = strlen($kode_jemaat) + 1;
                $select_hubkel = "(IFNULL(MAX(CAST(SUBSTRING(kode_jemaat, ".$jml_start.", 3) AS UNSIGNED)), 0) + 1) nomor_urut";
                $where_hubkel = array('active' => 1, 'id_sektor' => $id_sektor, 'id_jemaat_parent' => $id_jemaat_parent, 'id_hubungan_keluarga' => $id_hubungan_keluarga);
                $data_send_hubkel = array('where' => $where_hubkel, 'select' => $select_hubkel);
                $load_data_hubkel = $this->jemaat->load_data($data_send_hubkel);
                if($load_data_hubkel->num_rows() > 0){
                    $nomor_urut = $load_data_hubkel->row()->nomor_urut;
                    $kode_jemaat .= $nomor_urut;
                }
            }
        }

        return $kode_jemaat;
    }

    function lang($pesan){
        $hasil = $this->lang->line($pesan);
        return ($hasil ? $hasil : '{{ '.$pesan. ' }}');
    }
}
?>
