<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_admin_model extends MY_Model{

    protected $table = 'mst_admin';

//    public function load_data($receive_data = array()){
//        $this->db->select("id_admin,
//                        nama_admin,
//                        username_admin,
//                        status_admin,
//                        id_jns_admin,
//                        (select jns_admin from jns_admin where id_jns_admin=mst_admin.id_jns_admin) jns_admin
//                        ", false);
//
//        if (array_key_exists('where', $receive_data)) {
//            $this->db->where($receive_data['where']);
//        }
//        if (array_key_exists('like', $receive_data)) {
//            $this->db->or_like($receive_data['like']);
//        }
//        if (array_key_exists('order', $receive_data)) {
//            $this->db->order_by($receive_data['order']);
//        }
//        if(array_key_exists('limit', $receive_data)){
//            $limit = explode(",", $receive_data['limit']);
//            $this->db->limit($limit[0], $limit[1]);
//        }
//
//        $table = $this->db->get($this->table);
//        return $table;
//    }


    public function login($username, $password, $from = ''){
        $this->db->select('id_admin,
                        nama_admin,
                        username_admin,
                        foto_admin,
                        status_admin,
                        mst_admin.id_jns_admin,
                        jns_admin');
        $this->db->from('mst_admin');
        $this->db->where('username_admin',$username);
        $this->db->where('password_admin',md5($password));
        $this->db->where('status_admin','A');
        $this->db->join('jns_admin', 'jns_admin.id_jns_admin = mst_admin.id_jns_admin', 'left');
        $tabel = $this->db->get();

        $return = false;
        if($tabel->num_rows()==1){
            $tabel = $tabel->row();

            if($from == 'mobile'){
                $status = true;
                $return = array('status' => $status, 'data' => $tabel);
            }
            else{
                $session = array("id_admin" => $tabel->id_admin,
                    "nama_admin"		    => $tabel->nama_admin,
                    "username_admin"	    => $tabel->username_admin,
                    "password_admin"	    => md5($password),
                    "id_jns_admin"          => $tabel->id_jns_admin,
                    "foto_admin"          => $tabel->foto_admin,
                    "jns_admin"          => $tabel->jns_admin,
                    "login_as"	            => "administrator"
                );
                $this->session->set_userdata($session);
                $return = true;
            }
        }

        return $return;
    }
    public function checkAdmin($username, $password){
        $this->db->select('*');
        $this->db->from('mst_admin');
        $this->db->where('username_admin',$username);
        $this->db->where('password_admin',$password);
        $this->db->where('status_admin','A');
        $data = $this->db->get();

        if($data->num_rows()==1)
            return true;
        else
            return false;

    }
}

?>
