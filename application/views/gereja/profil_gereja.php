<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Profil Gereja
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="form_profil_gereja">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Profil Gereja
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_profil_gereja" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Profil_gereja/simpan" method="post" autocomplete="off">
                                <input type="hidden" class="form-control" id="id_profil_gereja" name="id_profil_gereja" maxlength="11" placeholder="">
							<div class="form-group row">
                                <label class="<?php echo $kolom_label; ?> col-form-label">Kode Gereja <?php echo $red_star; ?></label>
                                <div class="col-sm-3">
                                    <input type="hidden" id="kode_gereja_asli" name="kode_gereja_asli">
                                    <input type="text" autocomplate="off" class="form-control" id="kode_gereja" name="kode_gereja" maxlength="10" placeholder="" required>
                                </div>
                            </div>
                                <div class="form-group row">
                                <label class="<?php echo $kolom_label; ?> col-form-label">Nama Gereja <?php echo $red_star; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" autocomplate="off" class="form-control" id="nama_gereja" name="nama_gereja" maxlength="200" placeholder="" required>
                                </div>
                            </div>
							<div class="form-group row">
                                <label class="<?php echo $kolom_label; ?> col-form-label">Alamat Gereja <?php echo $red_star; ?></label>
                                <div class="col-sm-9">
                                    <textarea style="height:100px;" class="form-control" id="alamat_gereja" name="alamat_gereja" placeholder="" required></textarea>
                                </div>
                            </div>
							<div class="form-group row">
                                <label class="<?php echo $kolom_label; ?> col-form-label">Nomor Telepon Gereja </label>
                                <div class="col-sm-3">
                                    <input type="text" autocomplate="off" class="form-control" id="nomor_telepon_gereja" name="nomor_telepon_gereja" maxlength="50" placeholder="" >
                                </div>
                                <label class="<?php echo $kolom_label; ?> col-form-label"><?php echo ucwords(lang('ketua majelis jemaat')); ?> <?php echo $red_star; ?></label>
                                <div class="col-sm-3">
                                    <input type="text" autocomplate="off" class="form-control" id="kmj" name="kmj" maxlength="200" placeholder="" required>
                                </div>
                            </div>
							<div class="form-group row">
                                <label class="<?php echo $kolom_label; ?> col-form-label">Logo Gereja </label>
                                <div class="col-sm-3">
                                    <input type="file" name="logo_gereja" id="logo_gereja" class="form-control" accept="image/x-png,image/gif,image/jpeg" onchange="display_gambar()">
                                    <img src="" id="img_viewer" name="img_viewer" class="mt-3" style="object-fit: cover; width: 150px; height: 150px;" <?php echo $no_photo; ?>>
                                    <input type="hidden" id="logo_gereja_blob" name="logo_gereja_blob">
                                </div>
                            </div>

                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                        </button>
                                        <button class="btn btn-secondary waves-effect waves-light" type="button" onclick="form_action('hide')">
                                                <span class="btn-label"><i class="la la-times"></i>
                                                </span>Batal
                                        </button>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>

    function display_gambar() {
        $("#simpan").attr('disabled', 'disabled');
        generate_img('#logo_gereja', '#img_viewer', function(result){
            $("#logo_gereja_blob").val(result);
            $("#img_viewer").show(200);
            $("#simpan").removeAttr('disabled');
        });
    }

    $("#input_form_profil_gereja").on('submit', function(e){
        e.preventDefault();

        var id_profil_gereja = $("#id_profil_gereja").val();
        var kode_gereja = $("#kode_gereja").val();
        var kode_gereja_asli = $("#kode_gereja_asli").val();
        var nama_gereja = $("#nama_gereja").val();
        var alamat_gereja = $("#alamat_gereja").val();
        var nomor_telepon_gereja = $("#nomor_telepon_gereja").val();
        var logo_gereja = $("#logo_gereja").val();
        var kmj = $("#kmj").val();

        var action = $("#action").val();

        if(!action  || !kode_gereja || !nama_gereja || !alamat_gereja || !kmj){
            <?php echo alert('kosong'); ?>
        }
        else if(kode_gereja_asli != kode_gereja){
            var pertanyaan = "Proses ini akan mengubah semua kode jemaat. Apakah anda ingin menlanjutkan proses ini?";

            konfirmasi(pertanyaan, function(){
                submit();
            });
        }
        else{
            submit();
        }
    });
    function submit(){
        preloader('show');
        jQuery("#input_form_profil_gereja").ajaxSubmit({
            success:  function(msg){
                var data = safelyParseJSON(msg);
                preloader('hide');

                if(data.sts == 1){
                    <?php echo alert('simpan_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    }

    var ajax_request;
    function load_data(){
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        preloader('show');
        ajax_request = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Profil_gereja/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(list_data){
                preloader('hide');
                //parse JSON...
                // list_data = safelyParseJSON(msg);

                if(list_data){
                    $("#kode_gereja, #kode_gereja_asli").val(list_data.kode_gereja);
                    $("#nama_gereja").val(list_data.nama_gereja);
                    $("#alamat_gereja").val(list_data.alamat_gereja);
                    $("#nomor_telepon_gereja").val(list_data.nomor_telepon_gereja);
                    $("#kmj").val(list_data.kmj);

                    var logo_gereja = '<?php echo base_url(); ?>'+list_data.logo_gereja;
                    image_view('#img_viewer', logo_gereja);
                }

            }

        });
    }
    load_data();
</script>
</body>
</html>
