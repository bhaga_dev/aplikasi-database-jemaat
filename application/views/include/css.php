<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

<!--end::Fonts -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<?php echo base_url(); ?>assets/plugins/global/plugins.bundle.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<?php echo base_url(); ?>assets/css/skins/header/base/light.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/skins/header/menu/light.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/skins/brand/light.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/skins/aside/light.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/additional.css?versi=<?php echo $versi; ?>" rel="stylesheet" type="text/css" />
