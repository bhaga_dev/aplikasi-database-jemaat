<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            <img src="<?php echo base_url(); ?>assets/images/lisensi.png" style="height: 26px; margin-top: -4px; margin-right: 10px;">  <?php echo $aplikasi; ?> | Versi <?php echo $versi; ?>
        </div>
        <div class="kt-footer__menu">
            <a href="http://www.bhaga.id" target="_blank" class="kt-footer__menu-link kt-link">Made with <i class="fa fa-heart" style="color: red"></i> by Bhaga</a>
        </div>
    </div>
</div>

<!-- end:: Footer -->
