<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Background Kartu Tanda Jemaat
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone" id="form_laporan">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Kartu Tanda Jemaat
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_laporan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>jemaat/ganti_background_kartu" method="post" autocomplete="off">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Desain Background <?php echo $red_star; ?></label>
                                    <div class="col-sm-5">
                                        <input type="file" name="desain_background" id="desain_background" class="form-control" accept="image/x-png,image/gif,image/jpeg" onchange="display_gambar()">
                                        <span class="m-form__help">Desain harus berukuran 339 x 213 pixel</span>
                                        <img src="<?php echo base_url(); ?>assets/uploads/kartu/background_kartu.jpg" id="img_viewer" name="img_viewer" class="mt-3" style="object-fit: cover; width: 339px; height: 213px; border-radius: 7px; border:1px solid #030303" <?php echo $no_photo; ?>>
                                        <input type="hidden" id="desain_background_blob" name="desain_background_blob">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                            <span class="btn-label"><i class="la la-eye"></i>
                                            </span>Tampilkan
                                        </button>
                                        <a href="<?php echo base_url(); ?>page/cetak_kartu_jemaat" class="btn btn-default waves-effect waves-light">
                                            <span class="btn-label"><i class="la la-arrow-left"></i>
                                            </span>Kembali
                                        </a>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    function display_gambar() {
        $("#simpan").attr('disabled', 'disabled');
        generate_img('#desain_background', '#img_viewer', function(result){
            $("#desain_background_blob").val(result);
            $("#img_viewer").show(200);
            $("#simpan").removeAttr('disabled');
        });
    }

    $("#input_form_laporan").on('submit', function(e){
        e.preventDefault();
        var desain_background = $("#desain_background").val();

        if(desain_background == ''){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        $("#input_form_laporan")[0].reset();
                        <?php echo alert('simpan_berhasil'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }

                }
            });
        }

    });
</script>
</body>
</html>
