<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Jemaat
                            </h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <button class="btn btn-primary m-btn m-btn--custom m-btn--icon tabel_zone" onclick="form_action('show')">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Tambah Jemaat
                                        </span>
                                    </span>
                                </button>
                                <button class="btn btn-white m-btn m-btn--custom m-btn--icon form_zone hidden" onclick="form_action('hide')" style="display: none">
                                    <span>
                                        <i class="la la-arrow-left"></i>
                                        <span>
                                            Kembali
                                        </span>
                                    </span>
                                </button>
                                <input type="hidden" id="tipe_action">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone hidden" id="form_jemaat">
                    <div class="row">
                        <div class="col-sm-9" id="form_jemaat-form_zone">
<!--                            Biodata Jemaat Area-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="biodata_jemaat">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Biodata Jemaat
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_jemaat" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Jemaat/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="id_jemaat" name="id_jemaat" maxlength="11" placeholder="">

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label"><?php echo ucwords(lang('sektor')); ?> <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="id_sektor" name="id_sektor" required>
                                                    <option value="">--Pilih Data--</option>
                                                    <?php
                                                    if($konten['sektor']->num_rows() > 0){
                                                        foreach($konten['sektor']->result() as $list){
                                                            echo '<option value="'.$list->id_sektor.'">'.$list->nama_sektor.'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <i title="Anda bisa menambahkan <?php _lang('sektor'); ?> pada menu <a href='<?php echo base_url(); ?>page/sektor'>Gereja > <?php echo ucwords(lang('sektor')); ?></a>" data-trigger="manual" data-html="true" class="fa fa-question-circle question_bubble" data-container="body" data-toggle="kt-tooltip" data-placement="right"></i>
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Hubungan Keluarga <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <input type="hidden" id="id_hubungan_keluarga_asli" name="id_hubungan_keluarga_asli">
                                                <select class="form-control select2" id="id_hubungan_keluarga" name="id_hubungan_keluarga" required>
                                                    <option value="">--Pilih Data--</option>
                                                    <?php
                                                    if($konten['hubungan_keluarga']->num_rows() > 0){
                                                        foreach($konten['hubungan_keluarga']->result() as $list){
                                                            echo '<option value="'.$list->id_hubungan_keluarga.'">'.$list->nama_hubungan_keluarga.'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="jemaat_parent_zone" style="display: none;">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Kepala Keluarga <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <input type="hidden" id="id_jemaat_parent_asli" name="id_jemaat_parent_asli">
                                                <select class="form-control" id="id_jemaat_parent" name="id_jemaat_parent">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Nama Lengkap <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="nama_jemaat" name="nama_jemaat" maxlength="200" placeholder="" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tgl Lahir </label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar"></i>
                                                </span>
                                                    </div>
                                                    <input type="text" autocomplate="off" class="form-control mask_tanggal" id="tgl_lahir" name="tgl_lahir" placeholder="">
                                                </div>
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tempat Lahir </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="tempat_lahir" name="tempat_lahir" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label"><?php echo ucwords(lang('pelkat')); ?> <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" id="id_pelkat" name="id_pelkat" required>
                                                    <option value="">-- Pilih Data --</option>
                                                    <?php
                                                    if($konten['pelkat']->num_rows() > 0){
                                                        foreach ($konten['pelkat']->result() as $pelkat){
                                                            echo '<option value="'.$pelkat->id_pelkat.'">'.$pelkat->nama_pelkat.($pelkat->singkatan_pelkat ? ' ('.$pelkat->singkatan_pelkat.')' : '').'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Jenis Kelamin <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="jenis_kelamin" name="jenis_kelamin" required>
                                                    <option value="">--Pilih Data--</option>
                                                    <option value="L">Laki-Laki</option>
                                                    <option value="P">Perempuan</option>
                                                </select>
                                            </div>
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Golongan Darah </label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="golongan_darah" name="golongan_darah">
                                                    <option value="">-- Pilih Data --</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="AB">AB</option>
                                                    <option value="O">O</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Alamat </label>
                                            <div class="col-sm-9">
                                                <textarea style="height:100px;" class="form-control" id="alamat" name="alamat" placeholder="" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">RT </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="rt" name="rt" maxlength="3" placeholder="" >
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">RW </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="rw" name="rw" maxlength="3" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Kelurahan <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" id="id_kelurahan" name="id_kelurahan" required></select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                                </button>
                                                <input type="hidden" id="biodata-action" name="action" value="save">
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Baptis Area-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="data_baptis" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Data Baptis
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_baptis" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Baptis/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="id_jemaat" id="baptis-id_jemaat" name="id_jemaat">

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Status Baptis <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="status_baptis" name="status_baptis">
                                                    <option value="">-- Pilih Data --</option>
                                                    <option value="B">Belum Baptis</option>
                                                    <option value="S">Sudah Baptis</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tgl Baptis </label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" autocomplate="off" class="form-control mask_tanggal" id="tgl_baptis" name="tgl_baptis" placeholder="">
                                                </div>
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tempat Baptis </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="tempat_baptis" name="tempat_baptis" maxlength="200" placeholder="" >
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Sidi Area-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="data_sidi" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Data Sidi
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_sidi" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Sidi/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="id_jemaat" id="sidi-id_jemaat" name="id_jemaat">
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Status Sidi <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">

                                                <select class="form-control select2" id="status_sidi" name="status_sidi">
                                                    <option value="">-- Pilih Data --</option>
                                                    <option value="B">Belum Sidi</option>
                                                    <option value="S">Sudah Sidi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tgl Sidi </label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" autocomplate="off" class="form-control mask_tanggal" id="tgl_sidi" name="tgl_sidi" placeholder="">
                                                </div>
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tempat Sidi </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="tempat_sidi" name="tempat_sidi" maxlength="200" placeholder="" >
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Perkawinan Area-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="data_perkawinan" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Data Perkawinan
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_perkawinan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Perkawinan/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="perkawinan-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Status Perkawinan <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="id_status_perkawinan" name="id_status_perkawinan" required>
                                                    <?php
                                                    if($konten['status_perkawinan']->num_rows() > 0){
                                                        foreach($konten['status_perkawinan']->result() as $list){
                                                            echo '<option value="'.$list->id_status_perkawinan.'">'.$list->nama_status_perkawinan.'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tgl Kawin Gereja </label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" autocomplate="off" class="form-control mask_tanggal" id="tgl_kawin_gereja" name="tgl_kawin_gereja" placeholder="">
                                                </div>
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Tgl Kawin Sipil </label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" autocomplate="off" class="form-control mask_tanggal" id="tgl_kawin_sipil" name="tgl_kawin_sipil" placeholder="">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data pendidikan-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="pendidikan" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Pendidikan
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_pendidikan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Pendidikan/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="pendidikan-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Jenjang Pendidikan <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="id_jenjang_pendidikan" name="id_jenjang_pendidikan" required>
                                                    <?php
                                                    if($konten['jenjang_pendidikan']->num_rows() > 0){
                                                        foreach($konten['jenjang_pendidikan']->result() as $list){
                                                            echo '<option value="'.$list->id_jenjang_pendidikan.'">'.$list->nama_jenjang_pendidikan.'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Jurusan Pendidikan </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="jurusan_pendidikan" name="jurusan_pendidikan" maxlength="30" placeholder="" >
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Gelar </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="gelar" name="gelar" maxlength="100" placeholder="" >
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Pekerjaan-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="pekerjaan" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Pekerjaan
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_pekerjaan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Pekerjaan/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="pekerjaan-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Pekerjaan Saat Ini</label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="nama_pekerjaan" name="nama_pekerjaan" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Kantor / Institusi </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="kantor_institusi" name="kantor_institusi" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Posisi / Jabatan </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="posisi_jabatan" name="posisi_jabatan" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Profesi </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="profesi" name="profesi" maxlength="200" placeholder="" >
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Kontak-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="kontak" style="display: none">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Kontak
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_kontak" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Kontak/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="kontak-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Telepon Rumah </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="telepon_rumah" name="telepon_rumah" maxlength="10" placeholder="" >
                                            </div>

                                            <label class="<?php echo $kolom_label; ?> col-form-label">Nomor Hp </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="nomor_hp" name="nomor_hp" maxlength="15" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Email </label>
                                            <div class="col-sm-3">
                                                <input type="text" autocomplate="off" class="form-control" id="email" name="email" maxlength="200" placeholder="" >
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data Pengalaman & Keahlian-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="pengalaman_keahlian" style="display: none;">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Pengalaman & Keahlian
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_pengalaman_keahlian" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Pengalaman_keahlian/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="pengalaman_keahlian-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Pengalaman Gereja </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="pengalaman_gereja" name="pengalaman_gereja" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Penguasaan Bahasa </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="penguasaan_bahasa" name="penguasaan_bahasa" maxlength="200" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Keahlian </label>
                                            <div class="col-sm-9">
                                                <input type="text" autocomplate="off" class="form-control" id="keahlian" name="keahlian" maxlength="200" placeholder="" >
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<!--                            Data lain-lain-->
                            <div class="kt-portlet kt-portlet--mobile form_jemaat" id="lain_lain" style="display: none;">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Lain Lain
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <form id="input_form_jemaat_lain_lain" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Lain_lain/simpan" method="post" autocomplete="off">
                                        <input type="hidden" class="form-control id_jemaat" id="lain_lain-id_jemaat" name="id_jemaat" maxlength="11" placeholder="">

                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Riwayat </label>
                                            <div class="col-sm-9">
                                                <textarea style="height:100px;" class="form-control" id="riwayat" name="riwayat" placeholder="" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Catatan </label>
                                            <div class="col-sm-9">
                                                <textarea style="height:100px;" class="form-control" id="catatan" name="catatan" placeholder="" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Status Jemaat <?php echo $red_star; ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control select2" id="status_jemaat" name="status_jemaat" required>
                                                    <option value="">-- Pilih Data --</option>
                                                    <option value="1">Aktif</option>
                                                    <option value="0">Tidak Aktif</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="<?php echo $kolom_label; ?>"></div>
                                            <div class="col-sm-9">
                                                <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                    <span class="btn-label"><i class="la la-save"></i>
                                                    </span>Simpan
                                                </button>
                                                <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

<!--                        Menu Navigasi-->
                        <div class="col-sm-3" id="form_jemaat-menu_zone" style="display: none">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-section" style="margin: unset">
                                    <div class="kt-section__content kt-section__content--border kt-section__content--fit">
                                        <ul class="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3" role="tablist">
                                            <li class="kt-nav__item active" id="menu-biodata_jemaat">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('biodata_jemaat')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/biodata_jemaat.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">1. Biodata Jemaat</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-data_baptis">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('data_baptis')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/baptism.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">2. Data Baptis</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-data_sidi">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('data_sidi')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/sidi.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">3. Data Sidi</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-data_perkawinan">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('data_perkawinan')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/marriage.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">4. Data Perkawinan</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-pendidikan">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('pendidikan')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/education.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">5. Pendidikan</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-pekerjaan">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('pekerjaan')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/briefcase.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">6. Pekerjaan</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-kontak">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('kontak')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/phone-receiver.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">7. Kontak</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-pengalaman_keahlian">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('pengalaman_keahlian')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/user-experience.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">8. Pengalaman & Keahlian</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item" id="menu-lain_lain">
                                                <a class="kt-nav__link" href="#" onclick="navigasi('lain_lain')">
                                                        <span class="kt-nav__link-icon">
                                                            <img src="<?php echo base_url(); ?>assets/images/icons/light-bulb.svg" style="height: 20px;">
                                                        </span>
                                                    <span class="kt-nav__link-text">9. Lain-Lain</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tabel_zone" id="tabel_jemaat">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data Jemaat
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div id="data_zona_jemaat">
                                <div class="kt-input-icon kt-input-icon--left mb-3">
                                    <input type="text" autocomplete="off" class="form-control form-control-filter" id="filter" name="filter" placeholder="Masukkan kata kunci pencarian">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>


                                <div class="table-responsive" style="min-height: 200px;">
                                    <input type="hidden" id="tipe_order" value="ASC">
                                    <table class="table table-sm table-striped" id="data_jemaat">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th class="kt-font-primary" style="cursor:pointer;" id="order_data">Kode Jemaat <img src="<?php echo base_url(); ?>assets/images/icons/a_z.svg" style="height: 15px;" id="icon_sort"></th>
                                            <th>Nama Jemaat</th>
                                            <th>Hubungan Keluarga</th>
                                            <th>Sektor</th>
                                            <th>Jns. Kelamin</th>
                                            <th><?php echo ucwords(lang('pelkat')); ?></th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kt-datatable kt-datatable--default">
                                    <div class="kt-datatable__pager kt-datatable--paging-loaded">
                                        <input type="hidden" id="page" name="page" value="1">
                                        <input type="hidden" id="last_page" name="last_page">
                                        <input type="hidden" id="last_page_status" name="last_page_status" value="false">
                                        <ul class="kt-datatable__pager-nav">
                                            <li>
                                                <a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first" onclick="page('first')">
                                                    <i class="flaticon2-fast-back"></i>
                                                </a>
                                            </li>
                                            <li id="info_halaman">
                                                <a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page('previous')">
                                                    <i class="flaticon2-back"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" onclick="page('next')">
                                                    <i class="flaticon2-next"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" onclick="page('last')">
                                                    <i class="flaticon2-fast-next"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>
<div class="modal fade" id="ganti_kk_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Ganti Kepala Keluarga
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div>Anda harus memilih kepala keluarga yang baru untuk anggota keluarga <span id="ganti_kk-nama_kepala_keluarga" class="font-weight-bold"></span></div>
                <div class="mt-4 mb-2">Kepala Keluarga Baru <?php echo $red_star; ?></div>
                <select class="form-control" id="ganti_kk-id_jemaat_parent_baru" name="id_jemaat_parent_baru">
                    <option value=""></option>
                </select>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="ganti_kk-id_jemaat_parent_lama">
                <button type="button" class="btn btn-success" data-dismiss="modal" id="simpan_ganti_kk">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('form').bind("keydown", function(e) {
        if ((e.keyCode == 13)  && ($(e.target)[0]!=$("textarea")[0]) ) {
            if($("button").is(":focus")){
                var btn_element = $(document.activeElement);
                $(btn_element).trigger('click');
            }
        }
    });
    $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            form_action('hide');
        }
    });

    //Shortcut
    $(document).keypress(function(e) {
        console.log(e.which);
        if ( e.shiftKey && ( e.which === 43 ) ) { // shift dan +
            form_action('show');
            setTimeout(function () {
                $("#id_sektor").focus();
            }, 500);
        }
        if ( e.shiftKey && ( e.which === 33 ) ) { // shift dan 1
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('biodata_jemaat');
        }
        if ( e.shiftKey && ( e.which === 64 ) ) { // shift dan 2
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('data_baptis');
        }
        if ( e.shiftKey && ( e.which === 35 ) ) { // shift dan 3
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('data_sidi');
        }
        if ( e.shiftKey && ( e.which === 36 ) ) { // shift dan 4
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('data_perkawinan');
        }
        if ( e.shiftKey && ( e.which === 37 ) ) { // shift dan 5
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('pendidikan');
        }
        if ( e.shiftKey && ( e.which === 94 ) ) { // shift dan 6
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('pekerjaan');
        }
        if ( e.shiftKey && ( e.which === 38 ) ) { // shift dan 7
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('kontak');
        }
        if ( e.shiftKey && ( e.which === 42 ) ) { // shift dan 8
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('pengalaman_keahlian');
        }
        if ( e.shiftKey && ( e.which === 40 ) ) { // shift dan 9
            var action = $("#biodata-action").val();
            if(action == 'update')
                navigasi('lain_lain');
        }
    });

    $("#order_data").click(function(){
        var tipe_order = $("#tipe_order").val();
        if(tipe_order == 'ASC'){
            tipe_order = 'DESC';
            $("#icon_sort").attr('src', '<?php echo base_url(); ?>assets/images/icons/z_a.svg');
        }
        else{
            tipe_order = 'ASC';
            $("#icon_sort").attr('src', '<?php echo base_url(); ?>assets/images/icons/a_z.svg');
        }
        $("#tipe_order").val(tipe_order);

        $("#last_page_status").val('false');
        $("#page").val(1);
        load_data();
    });

    $(".mask_tanggal").inputmask("99-99-9999", {
        "placeholder": "dd-mm-yyyy"
    });
    $("#nomor_hp").inputmask("9999-9999-99999", {
        "placeholder": "    -    -    "
    });

    function navigasi(el){
        $(".form_jemaat").hide();
        $("#"+el).show(200)

        $(".kt-nav__item").removeClass('active');
        $("#menu-"+el).addClass('active');
    }

    function form_action(action){
        //reset form...
        $("#input_form_jemaat")[0].reset();
        $("#input_form_baptis")[0].reset();
        $("#input_form_sidi")[0].reset();
        $("#input_form_perkawinan")[0].reset();
        $("#input_form_pendidikan")[0].reset();
        $("#input_form_pekerjaan")[0].reset();
        $("#input_form_kontak")[0].reset();
        $("#input_form_pengalaman_keahlian")[0].reset();
        $("#input_form_jemaat_lain_lain")[0].reset();

        navigasi('biodata_jemaat');
        $("#biodata-action").val('save');
        $(".select2").val('').trigger('change');
        $("#id_jemaat_parent").val('').trigger('change');
        $("#jemaat_parent_zone, #form_jemaat-menu_zone").hide();
        $("#form_jemaat-form_zone").removeClass();
        $("#form_jemaat-form_zone").addClass('col-sm-12');

        $("#id_jemaat_parent_asli, #id_hubungan_asli").val('');

        $("#tipe_action").val('');

        if(action == 'show'){
            $(".form_zone").fadeIn(300);
            $(".tabel_zone").hide();
            $("#tipe_action").val('data_baru');
        }
        else{
            $(".form_zone").hide();
            $(".tabel_zone").fadeIn(300);
        }
    }

    $("#id_hubungan_keluarga").change(function(){
        var id_hubungan_keluarga = $(this).val();
        if(id_hubungan_keluarga == 1){
            $("#id_jemaat_parent").val('').trigger('change');
            $("#jemaat_parent_zone").hide(200);
        }
        else{
            $("#jemaat_parent_zone").show(200);
        }
    });

    $('#id_jemaat_parent').select2({
        minimumInputLength: 2,
        placeholder: "Pilih nama kepala keluarga disini...",
        // allowClear: false,
        ajax: {
            dataType: 'json',
            url: '<?php echo base_url(); ?>jemaat/load_select2',
            type: "GET",
            quietMillis: 50,
            data: function(term) {
                return {
                    filter: term,
                    id_hubungan_keluarga: '1',
                    token: '<?php echo genToken('LOAD_DATA'); ?>'
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    });
    $('#id_kelurahan').select2({
        minimumInputLength: 2,
        placeholder: "Pilih kelurahan disini...",
        // allowClear: false,
        ajax: {
            dataType: 'json',
            url: '<?php echo base_url(); ?>kelurahan/load_select2',
            type: "GET",
            quietMillis: 50,
            data: function(term) {
                return {
                    filter: term,
                    token: '<?php echo genToken('LOAD_DATA'); ?>'
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    });
    $('#ganti_kk-id_jemaat_parent_baru').select2({
        minimumInputLength: 2,
        placeholder: "Pilih nama kepala keluarga disini...",
        // allowClear: false,
        ajax: {
            dataType: 'json',
            url: '<?php echo base_url(); ?>jemaat/load_select2',
            type: "GET",
            quietMillis: 50,
            data: function(term) {
                return {
                    filter: term,
                    token: '<?php echo genToken('LOAD_DATA'); ?>'
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    });

    var list_data;
    $("#input_form_jemaat").on('submit', function(e){
        e.preventDefault();

        var id_jemaat = $("#id_jemaat").val();
        var nama_jemaat = $("#nama_jemaat").val();
        var id_sektor = $("#id_sektor").val();
        var id_hubungan_keluarga = $("#id_hubungan_keluarga").val();
        var id_hubungan_keluarga_asli = $("#id_hubungan_keluarga_asli").val();
        var id_jemaat_parent = $("#id_jemaat_parent").val();
        var id_jemaat_parent_asli = $("#id_jemaat_parent_asli").val();
        var jenis_kelamin = $("#jenis_kelamin").val();
        var tgl_lahir = $("#tgl_lahir").val();
        var id_pelkat = $("#id_pelkat").val();
        var tempat_lahir = $("#tempat_lahir").val();
        var golongan_darah = $("#golongan_darah").val();
        var alamat = $("#alamat").val();
        var rt = $("#rt").val();
        var rw = $("#rw").val();
        var id_kelurahan = $("#id_kelurahan").val();

        var action = $("#biodata-action").val();

        if(!action  || !nama_jemaat || !id_sektor || !id_hubungan_keluarga || !jenis_kelamin || !id_kelurahan || !id_pelkat){
            <?php echo alert('kosong'); ?>
        }
        else if(!isDate(tgl_lahir)){
            <?php echo alert('format_tgl_salah'); ?>
        }
        else if(action == 'update' && ((id_jemaat_parent != id_jemaat_parent_asli) || (id_hubungan_keluarga != id_hubungan_keluarga_asli))){
            var pertanyaan = "Proses ini akan mengubah kode jemaat. Apakah anda ingin melanjutkan?";

            konfirmasi(pertanyaan, function(){
                submit_jemaat();
            });
        }
        else{
            submit_jemaat();
        }
    });
    function submit_jemaat(){
        preloader('show');
        jQuery("#input_form_jemaat").ajaxSubmit({
            success:  function(msg){
                var data = safelyParseJSON(msg);
                preloader('hide');

                if(data.sts == 1){
                    var action = $("#biodata-action").val();
                    $(".id_jemaat").val(data.id);
                    //hapus seluruh field...
                    $("#last_page_status").val('false');
                    $("#page").val(1);
                    $("#biodata-action").val('update');

                    var id_jemaat_parent = $("#id_jemaat_parent").val();
                    var id_hubungan_keluarga = $("#id_hubungan_keluarga").val();
                    $("#id_jemaat_parent_asli").val(id_jemaat_parent);
                    $("#id_hubungan_keluarga_asli").val(id_hubungan_keluarga);

                    //load data..
                    load_data();
                    <?php echo alert('simpan_berhasil'); ?>

                    $("#form_jemaat-form_zone").removeClass();
                    $("#form_jemaat-form_zone").addClass('col-sm-9');
                    $("#form_jemaat-menu_zone").show();

                    $("html, body").animate({ scrollTop: 0 }, "slow");

                    if(action == 'save'){
                        navigasi('data_baptis');
                    }
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    }

    function edit(index, index_anggota_keluarga){
        form_action('show');
        $("#form_jemaat-form_zone").removeClass();
        $("#form_jemaat-form_zone").addClass('col-sm-9');
        $("#form_jemaat-menu_zone").show();
        if(index_anggota_keluarga != null){
            var data = list_data[index].anggota_keluarga[index_anggota_keluarga];
        }
        else{
            var data = list_data[index];
        }
        $("#id_jemaat, .id_jemaat").val(decodeHtml(data.id_jemaat));
        if(data.id_jemaat_parent != 0){
            var parent = data.parent;

            $("#id_jemaat_parent").append('<option value="'+parent.id_jemaat+'">'+parent.nama_jemaat+' - '+parent.nama_sektor+'</option>');
            $("#id_jemaat_parent, #id_jemaat_parent_asli").val(parent.id_jemaat);
            $("#id_jemaat_parent").trigger('change');
        }
        $("#nama_jemaat").val(decodeHtml(data.nama_jemaat));
        $("#id_sektor").val(decodeHtml(data.id_sektor)).trigger('change');
        $("#id_pelkat").val(decodeHtml(data.id_pelkat)).trigger('change');
        $("#id_hubungan_keluarga").val(decodeHtml(data.id_hubungan_keluarga)).trigger('change');
        $("#id_hubungan_keluarga_asli").val(decodeHtml(data.id_hubungan_keluarga));
        $("#jenis_kelamin").val(decodeHtml(data.jenis_kelamin)).trigger('change');
        $("#tgl_lahir").val(reformatDate(data.tgl_lahir, false, 'DD-MM-YYYY'));
        $("#tempat_lahir").val(decodeHtml(data.tempat_lahir));
        $("#golongan_darah").val(decodeHtml(data.golongan_darah)).trigger('change');
        $("#alamat").val(decodeHtml(data.alamat));
        $("#rt").val(decodeHtml(data.rt));
        $("#rw").val(decodeHtml(data.rw));
        $("#id_kelurahan").val(decodeHtml(data.id_kelurahan)).trigger('change');


        $("#id_kelurahan").append('<option value="'+data.id_kelurahan+'">'+data.nama_kelurahan+' - '+data.nama_kecamatan+' - '+data.nama_kabupaten_kota+' - '+data.nama_provinsi+'</option>');
        $("#id_kelurahan").trigger('change');

        //baptis
        var baptis = data.baptis;
        if(baptis){
            $("#status_baptis").val(baptis.status_baptis).trigger('change');
            $("#tgl_baptis").val(reformatDate(baptis.tgl_baptis, false, 'DD-MM-YYYY'));
            $("#tempat_baptis").val(baptis.tempat_baptis);
        }

        //sidi
        var sidi = data.sidi;
        if(sidi){
            $("#status_sidi").val(sidi.status_sidi).trigger('change');
            $("#tgl_sidi").val(reformatDate(sidi.tgl_sidi, false, 'DD-MM-YYYY'));
            $("#tempat_sidi").val(sidi.tempat_sidi);
        }

        //Perkawinan
        var perkawinan = data.perkawinan;
        if(perkawinan){
            $("#id_status_perkawinan").val(perkawinan.id_status_perkawinan).trigger('change');
            $("#tgl_kawin_gereja").val(reformatDate(perkawinan.tgl_kawin_gereja, false, 'DD-MM-YYYY'));
            $("#tgl_kawin_sipil").val(reformatDate(perkawinan.tgl_kawin_sipil, false, 'DD-MM-YYYY'));
        }

        //Pendidikan
        var pendidikan = data.pendidikan;
        if(pendidikan){
            $("#id_jenjang_pendidikan").val(pendidikan.id_jenjang_pendidikan).trigger('change');
            $("#jurusan_pendidikan").val(pendidikan.jurusan_pendidikan);
            $("#gelar").val(pendidikan.gelar);
        }

        //Pekerjaan
        var pekerjaan = data.pekerjaan;
        if(pekerjaan){
            $("#nama_pekerjaan").val(pekerjaan.nama_pekerjaan);
            $("#kantor_institusi").val(pekerjaan.kantor_institusi);
            $("#posisi_jabatan").val(pekerjaan.posisi_jabatan);
            $("#profesi").val(pekerjaan.profesi);
        }

        //Kontak
        var kontak = data.kontak;
        if(kontak){
            $("#telepon_rumah").val(kontak.telepon_rumah);
            $("#nomor_hp").val(kontak.nomor_hp);
            $("#email").val(kontak.email);
        }

        //Pengalaman & Keahlian
        var pengalaman_keahlian = data.pengalaman_keahlian;
        if(pengalaman_keahlian){
            $("#pengalaman_gereja").val(pengalaman_keahlian.pengalaman_gereja);
            $("#penguasaan_bahasa").val(pengalaman_keahlian.penguasaan_bahasa);
            $("#keahlian").val(pengalaman_keahlian.keahlian);
        }

        //Lain lain
        var lain_lain = data.lain_lain;
        if(lain_lain){
            $("#riwayat").val(lain_lain.riwayat);
            $("#catatan").val(lain_lain.catatan);
            $("#status_jemaat").val(lain_lain.status_jemaat).trigger('change');
        }

        $("#biodata-action").val('update');
        $("#tipe_action").val('');

    }
    function hapus(index){
        var data = list_data[index];
        var pertanyaan = "Apakah anda yakin ingin menghapus data "+data.nama_jemaat+"?";

        konfirmasi(pertanyaan, function(){
            if(data.id_hubungan_keluarga == 1){
                $("#ganti_kk-nama_kepala_keluarga").html(data.nama_jemaat);
                $("#ganti_kk-id_jemaat_parent_lama").val(data.id_jemaat);
                $("#ganti_kk-id_jemaat_parent_baru").val('').trigger('change');
                $("#ganti_kk_modal").modal('show');
            }
            else
                proses_hapus(data.id_jemaat);
        });
    }
    function proses_hapus(id){
        //show loading animation...
        preloader('show');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_jemaat'] = id;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Jemaat/hapus',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('hapus_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }

        });
    }

    function page(tipe){
            if(tipe == 'first'){
                $("#last_page_status").val('false');
                $("#page").val(1);
                load_data();
            }
            else if(tipe == 'previous'){
                var page = parseInt($("#page").val());
                var previous_page = page - 1;
                previous_page = (previous_page <= 1 ? 1 : previous_page);
                if(page != previous_page){
                    $("#last_page_status").val('false');
                    $("#page").val(previous_page);
                    load_data();
                }
            }
            else if(tipe == 'next'){
                var last_page_status = $("#last_page_status").val();
                var page = parseInt($("#page").val());
                var next_page = page + 1;
                if(last_page_status == "false"){
                    $("#page").val(next_page);
                    load_data();
                }
            }
            else if(tipe == 'last'){
                var last_page = parseInt($("#last_page").val());
                var page = parseInt($("#page").val());
                if(last_page != page){
                    $("#page").val(last_page);
                    load_data();
                }
            }
            else{
                $("#page").val(tipe);
                load_data();

            }
        }

    $("#filter").keyup(function(){
            $("#last_page_status").val('false');
            $("#page").val(1);
            ajax_request.abort();
            load_data();
        });

    var ajax_request;
    function load_data(){
        var page = $("#page").val();
        var jml_data = 10;

        var tipe_order = $("#tipe_order").val();
        var filter = $("#filter").val();

        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['page'] = page;
        data['jml_data'] = jml_data;
        data['filter'] = filter;
        data['tipe_order'] = tipe_order;


        elementLoading('show', '#data_jemaat');
        ajax_request = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Jemaat/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                elementLoading('hide', '#data_jemaat');
                //parse JSON...
                var result = safelyParseJSON(msg);
                    $("#last_page").val(result.last_page);
                    list_data = result.result;
                    info_halaman('#info_halaman', page, result.last_page);

                var rangkai = '';
                if(list_data.length > 0){
                    for(var i=0; i < list_data.length; i++){
                        rangkai += rangkai_tabel(list_data[i], i, null, page, jml_data);
                    }
                }
                if(list_data.length < jml_data)
                    $("#last_page_status").val('true');


                if(rangkai){
                    $("#empty_state").remove();
                    $("#data_zona_jemaat").show();

                    $("#data_jemaat tbody").html(rangkai);
                }
                else{
                    if(page == 1 && filter == '')
                        create_empty_state("#data_zona_jemaat");
                    else
                        $("#data_jemaat tbody").html('');
                }

            }

        });
    }
    load_data();

    function rangkai_tabel(list_data, i, a, page, jml_data){
        var token = '<?php echo genToken('LOAD_DATA'); ?>';
        var rangkai = '';
        var lain = list_data.lain_lain;
        var status_jemaat = render_badge('kt-badge--danger', 'Tidak Aktif');
        if(lain){
            if(lain.status_jemaat == 1){
                status_jemaat = render_badge('kt-badge--success', 'Aktif');
            }
        }

        var no = '';
        if(page && jml_data){
            no = (((page - 1) * jml_data) + i+1);
        }
        var cetak_kwj = '';
        if(list_data.id_hubungan_keluarga == 1){
            var url = '<?php echo base_url(); ?>laporan/kwj?kode_kk='+list_data.id_jemaat+'&token='+token;
            cetak_kwj = '<a class="dropdown-item" href="'+url+'" target="_blank"><i class="la la-credit-card"></i> Kartu Warga Jemaat</a>';
        }
        rangkai += '<tr>' +
                        '<td>'+no+'</td>' +
                        '<td>'+coverMe(list_data.kode_jemaat)+'</td>' +
                        '<td>'+coverMe(list_data.nama_jemaat)+'</td>' +
                        '<td>'+coverMe(list_data.nama_hubungan_keluarga)+'</td>' +
                        '<td>'+coverMe(list_data.nama_sektor)+'</td>' +
                        '<td>'+jns_kelamin(list_data.jenis_kelamin)+'</td>' +
                        '<td>'+list_data.singkatan_pelkat+'</td>' +
                        '<td>'+status_jemaat+'</td>' +
                        '<td>' +
                            '<div class="dropdown">' +
                                '<button class="btn btn-outline-hover-info btn-elevate btn-circle btn-icon" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                    '<i class="la la-ellipsis-h" style="color: #5578eb; font-size: 25px;"></i>' +
                                '</button>' +
                                '<div class="dropdown-menu dropdown-menu-right" >' +
                                    '<a class="dropdown-item" href="<?php echo base_url(); ?>jemaat/profil_jemaat?kode_jemaat='+list_data.id_jemaat+'&token='+token+'" target="_blank"><i class="la la-user"></i> Detail Jemaat</a>' +
                                    cetak_kwj+
                                    '<a class="dropdown-item" href="#" onclick="edit('+i+', '+a+')"><i class="la la-edit"></i> Edit</a>' +
                                    '<a class="dropdown-item kt-font-danger" href="#" onclick="hapus('+i+', '+a+')"><i class="la la-trash kt-font-danger"></i> Hapus</a>' +
                                '</div>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';

        if(list_data.anggota_keluarga){
            var anggota_keluarga = list_data.anggota_keluarga;
            for(var a = 0; a < anggota_keluarga.length; a++){
                rangkai += rangkai_tabel(anggota_keluarga[a], i, a);
            }
        }

        return rangkai;
    }


    $("#input_form_baptis").on('submit', function(e){
        e.preventDefault();

        var id_jemaat = $("#baptis-id_jemaat").val();
        var status_baptis = $("#status_baptis").val();
        var tgl_baptis = $("#tgl_baptis").val();
        var tempat_baptis = $("#tempat_baptis").val();

        if(!id_jemaat || !status_baptis){
            <?php echo alert('kosong'); ?>
        }
        else if(!isDate(tgl_baptis)){
            <?php echo alert('format_tgl_salah'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>

                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('data_sidi');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });


        }
    });
    $("#input_form_sidi").on('submit', function(e){
        e.preventDefault();

        var id_jemaat = $("#sidi-id_jemaat").val();
        var id_sidi = $("#id_sidi").val();
        var status_sidi = $("#status_sidi").val();
        var tgl_sidi = $("#tgl_sidi").val();
        var tempat_sidi = $("#tempat_sidi").val();

        if(!id_jemaat || !status_sidi){
            <?php echo alert('kosong'); ?>
        }
        else if(!isDate(tgl_sidi)){
            <?php echo alert('format_tgl_salah'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('data_perkawinan');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });
    $("#input_form_perkawinan").on('submit', function(e){
        e.preventDefault();

        var id_perkawinan = $("#id_perkawinan").val();
        var id_jemaat = $("#perkawinan-id_jemaat").val();
        var id_status_perkawinan = $("#id_status_perkawinan").val();
        var tgl_kawin_gereja = $("#tgl_kawin_gereja").val();
        var tgl_kawin_sipil = $("#tgl_kawin_sipil").val();

        if(!id_jemaat || !id_status_perkawinan){
            <?php echo alert('kosong'); ?>
        }
        else if(!isDate(tgl_kawin_gereja)){
            <?php echo alert('format_tgl_salah'); ?>
        }else if(!isDate(tgl_kawin_sipil)){
            <?php echo alert('format_tgl_salah'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('pendidikan');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });


        }
    });
    $("#input_form_pendidikan").on('submit', function(e){
        e.preventDefault();

        var id_pendidikan = $("#id_pendidikan").val();
        var id_jemaat = $("#pendidikan-id_jemaat").val();
        var id_jenjang_pendidikan = $("#id_jenjang_pendidikan").val();
        var jurusan_pendidikan = $("#jurusan_pendidikan").val();
        var gelar = $("#gelar").val();

        if(!id_jemaat || !id_jenjang_pendidikan){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('pekerjaan');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });
    $("#input_form_pekerjaan").on('submit', function(e){
        e.preventDefault();

        var id_pekerjaan = $("#id_pekerjaan").val();
        var id_jemaat = $("#pekerjaan-id_jemaat").val();
        var nama_pekerjaan = $("#nama_pekerjaan").val();
        var kantor_institusi = $("#kantor_institusi").val();
        var posisi_jabatan = $("#posisi_jabatan").val();
        var profesi = $("#profesi").val();

        if(!id_jemaat){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('kontak');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });
    $("#input_form_kontak").on('submit', function(e){
        e.preventDefault();

        var id_kontak = $("#id_kontak").val();
        var id_jemaat = $("#kontak-id_jemaat").val();
        var telepon_rumah = $("#telepon_rumah").val();
        var nomor_hp = $("#nomor_hp").val();
        var email = $("#email").val();

        if(!id_jemaat){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('pengalaman_keahlian');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });
    $("#input_form_pengalaman_keahlian").on('submit', function(e){
        e.preventDefault();

        var id_pengalaman_keahlian = $("#id_pengalaman_keahlian").val();
        var id_jemaat = $("#pengalaman_keahlian-id_jemaat").val();
        var pengalaman_gereja = $("#pengalaman_gereja").val();
        var penguasaan_bahasa = $("#penguasaan_bahasa").val();
        var keahlian = $("#keahlian").val();

        if(!id_jemaat){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                        var tipe_action = $("#tipe_action").val();
                        if(tipe_action == 'data_baru'){
                            navigasi('lain_lain');
                        }
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });
    $("#input_form_jemaat_lain_lain").on('submit', function(e){
        e.preventDefault();

        var id_lain_lain = $("#id_lain_lain").val();
        var id_jemaat = $("#lain_lain-id_jemaat").val();
        var riwayat = $("#riwayat").val();
        var catatan = $("#catatan").val();
        var status_jemaat = convertToAngka($("#status_jemaat").val());

        if(!id_jemaat){
            <?php echo alert('kosong'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });

    $("#simpan_ganti_kk").click(function(){
        var id_jemaat_parent_lama = $("#ganti_kk-id_jemaat_parent_lama").val();
        var id_jemaat_parent_baru = $("#ganti_kk-id_jemaat_parent_baru").val();

        //show loading animation...
        preloader('show');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_jemaat_parent_lama'] = id_jemaat_parent_lama;
        data['id_jemaat_parent_baru'] = id_jemaat_parent_baru;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Jemaat/hapus_ganti_kk',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('simpan_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }

        });
    });
</script>
</body>
</html>
