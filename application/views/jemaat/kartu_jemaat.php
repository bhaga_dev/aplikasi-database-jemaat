<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Kartu Tanda Jemaat
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone" id="form_laporan">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Filter Jemaat
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_laporan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>laporan/kartu_jemaat" method="get" autocomplete="off" target="_blank">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Kepala Keluarga <?php echo $red_star; ?></label>
                                    <div class="col-sm-5">
                                        <select class="form-control select2" id="kode_kk" name="kode_kk" required>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                            <span class="btn-label"><i class="la la-eye"></i>
                                            </span>Tampilkan
                                        </button>
                                        <a href="<?php echo base_url(); ?>page/background_kartu" class="btn btn-default waves-effect waves-light">
                                            <span class="btn-label"><i class="la la-gear"></i>
                                            </span>Ganti Background Kartu
                                        </a>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('LOAD_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>

    $('#kode_kk').select2({
        minimumInputLength: 2,
        placeholder: "Pilih nama kepala keluarga disini...",
        // allowClear: false,
        ajax: {
            dataType: 'json',
            url: '<?php echo base_url(); ?>jemaat/load_select2',
            type: "GET",
            quietMillis: 50,
            data: function(term) {
                return {
                    filter: term,
                    id_hubungan_keluarga: '1',
                    token: '<?php echo genToken('LOAD_DATA'); ?>'
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    });
</script>
</body>
</html>
