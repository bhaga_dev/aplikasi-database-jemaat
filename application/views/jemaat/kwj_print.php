<?php

$nama_kk = '';
foreach ($konten['jemaat'] as $jemaat){
    if($jemaat->id_hubungan_keluarga == 1){
        $nama_kk = $jemaat->nama_jemaat;
    }
}
?>
<html lang="id"><head>
    <title>Kartu Warga Jemaat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        @media print {
            @page {size: landscape}

            body {
                padding: 0!important;
                margin: 0!important;
            }

            #action-area {
                display: none;
            }
        }

        @media screen and (min-width: 1025px) {
            .btn-download {
                display: none !important;
            }

            .btn-back {
                display: none !important;
            }
        }

        @media screen and (max-width: 1024px) {
            .content-area>div {
                width: auto !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 720px) {
            .content-area>div {
                width: auto !important;
            }
        }

        @media screen and (max-width: 420px) {
            .content-area>div {
                width: 790px !important;
            }
        }

        @media screen and (max-width: 430px) {
            .content-area {
                transform: scale(0.59) translate(-35%, -35%)
            }

            .content-area>div {
                width: 720px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 380px) {
            .content-area {
                transform: scale(0.45) translate(-58%, -62%);
            }

            .content-area>div {
                width: 790px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 320px) {
            .content-area>div {
                width: 700px !important;
            }
        }

        .border{ border: thin solid #979797; }
        .no_border_left{ border-left: unset; }
        .no_border_top{ border-top: unset; }
        table tbody tr td{ padding: 3px 0; }
        table thead tr th, .header td{ padding: 3px; text-align: center }
    </style>

<body id="lembar_invoice" style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">

    <div id="action-area">
        <div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 35px;">
            </div>
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">
                <a class="btn-print" href="<?php echo $konten['prev']; ?>" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button style="height: 100%; cursor: pointer;padding: 8px 40px;border: 1px solid #7400C8;border-radius: 8px;background-color: #FFF;margin-left: 16px;color: #7400C8;font-size: 12px;line-height: 1.333;font-weight: 700;">Sebelumnya</button>
                </a>
                <a class="btn-print" href="<?php echo $konten['next']; ?>" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button style="height: 100%; cursor: pointer;padding: 8px 40px;border: 1px solid #7400C8;border-radius: 8px;background-color: #FFF;margin-left: 16px;color: #7400C8;font-size: 12px;line-height: 1.333;font-weight: 700;">Selanjutnya</button>
                </a>
                <a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #7400C8;border-radius: 8px;background-color: #7400C8;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
                </a>
            </div>
        </div>
        <div id="extwaiokist" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW=="><div id="extwaiimpotscp" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW==" vn="0yten"></div></div>
    </div>

<div class="content-area">

    <div style="margin: auto;">
        <table style="width: 100%; padding: 25px 32px;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>
                    <!-- header -->
                    <table width="100%">
                        <tbody>
                        <tr valign="top">
                            <td style="text-align: left;">
                                    <table style="font-weight: bold; font-size: 10px">
                                        <tr valign="top">
                                            <td>Nama Keluarga</td>
                                            <td width="3%">:</td>
                                            <td><?php echo $nama_kk; ?></td>
                                        </tr>
                                        <tr valign="top">
                                            <td><?php echo ucwords(lang('sektor')); ?></td>
                                            <td width="3%">:</td>
                                            <td><?php echo $konten['jemaat'][0]->nama_sektor; ?></td>
                                        </tr>
                                        <tr valign="top">
                                            <td>Alamat</td>
                                            <td width="3%">:</td>
                                            <td>
                                                <?php echo coverMe($konten['jemaat'][0]->alamat, 'Tidak ada alamat').
                                                    ', RT/RW: '.coverMe($konten['jemaat'][0]->rt, '000').' / '.coverMe($konten['jemaat'][0]->rw, '000').
                                                    '<br>Kel. '.$konten['jemaat'][0]->nama_kelurahan.', Kec. '.$konten['jemaat'][0]->nama_kecamatan.', '.$konten['jemaat'][0]->nama_kabupaten_kota.', '.$konten['jemaat'][0]->nama_provinsi; ?>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>Kontak</td>
                                            <td width="3%">:</td>
                                            <td>
                                                <?php echo 'Telp. Rumah: '.coverMe($konten['jemaat'][0]->nomor_hp, '<i>Tidak ada</i>').
                                                    ', HP: '.coverMe($konten['jemaat'][0]->nomor_hp, '<i>Tidak ada</i>').', Email: '.coverMe($konten['jemaat'][0]->email, '<i>Tidak ada</i>'); ?>
                                            </td>
                                        </tr>
                                    </table>
                            </td>
                            <td>
                                <div style="font-weight: bold; font-size: 18px; text-align: right">
                                    <div>KARTU WARGA JEMAAT</div>
                                    <div><?php echo strtoupper($konten['profil_gereja']->nama_gereja); ?></div>
                                    <div>NOMOR KELUARGA: <?php echo $konten['jemaat'][0]->kode_jemaat; ?></div>
                                </div>
                            </td>
                            <td style="text-align: right; width: 30px;">
                                <img src="<?php echo base_url().$konten['profil_gereja']->logo_gereja; ?>" style="height: 70px; margin-left: 20px; width: auto" <?php echo $no_photo; ?>>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <!-- ringkasan belanja -->
            <tr>
                <td>
                    <table style="margin-top: 10px; font-size: 10px;" width="100%" cellspacing="0" cellpadding="10">
                        <thead>
                            <tr style="font-weight: bold">
                                <th style="width: 3%" class="border">No</th>
                                <th style="width: 10%" class="border no_border_left">Kode Jemaat</th>
                                <th style="width: 10%" class="border no_border_left">Hubungan Keluarga</th>
                                <th style="width: 13%" class="border no_border_left">Nama Jemaat</th>
                                <th style="width: 7%" class="border no_border_left">Jenis Kelamin</th>
                                <th style="width: 10%" class="border no_border_left">Tempat Lahir</th>
                                <th style="width: 9%" class="border no_border_left">Tgl. Lahir</th>
                                <th style="width: 10%" class="border no_border_left">Tempat Baptis</th>
                                <th style="width: 9%" class="border no_border_left">Tgl. Baptis</th>
                                <th style="width: 10%" class="border no_border_left">Tempat Sidi</th>
                                <th style="width: 9%" class="border no_border_left">Tgl. Sidi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($konten['jemaat'] as $jemaat){
                            echo '<tr style="text-align: center">
                                        <td class="border no_border_top">'.$no.'</td>
                                        <td class="border no_border_left no_border_top">'.$jemaat->kode_jemaat.'</td>
                                        <td class="border no_border_left no_border_top">'.$jemaat->nama_hubungan_keluarga.'</td>
                                        <td class="border no_border_left no_border_top">'.$jemaat->nama_jemaat.'</td>
                                        <td class="border no_border_left no_border_top">'.jenis_kelamin($jemaat->jenis_kelamin).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->tempat_lahir).'</td>
                                        <td class="border no_border_left no_border_top">'.($jemaat->tgl_lahir ? reformat_date(balik_tanggal($jemaat->tgl_lahir)) : '-').'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->tempat_baptis).'</td>
                                        <td class="border no_border_left no_border_top">'.($jemaat->tgl_baptis ? reformat_date(balik_tanggal($jemaat->tgl_baptis)) : '-').'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->tempat_sidi).'</td>
                                        <td class="border no_border_left no_border_top">'.($jemaat->tgl_sidi ? reformat_date(balik_tanggal($jemaat->tgl_sidi)) : '-').'</td>
                                    </tr>';
                            $no++;
                        }
                        $additional_row = '';
                        for($i = $no; $i <= 8; $i++){
                            $additional_row .= '<tr style="text-align: center">
                                        <td class="border no_border_top">'.$i.'</td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                        <td class="border no_border_left no_border_top"></td>
                                    </tr>';
                        }
                        echo $additional_row;
                        echo '<tr><td colspan="11"></td></tr>';
                        echo '<tr class="header" style="font-weight: bold">
                                <td style="width: 3%" class="border">No</td>
                                <td style="width: 10%" class="border no_border_left">Status Perkawinan</td>
                                <td style="width: 10%" class="border no_border_left">Tgl. Perkawinan Gereja</td>
                                <td style="width: 13%" class="border no_border_left">Tgl. Perkawinan Sipil</td>
                                <td style="width: 7%" class="border no_border_left">Gol. Darah</td>
                                <td style="width: 10%" class="border no_border_left">Pendidikan Terakhir</td>
                                <td style="width: 9%" class="border no_border_left">Gelar</td>
                                <td style="width: 10%" class="border no_border_left">Jurusan</td>
                                <td style="width: 9%" class="border no_border_left">Pekerjaan</td>
                                <td style="width: 10%" class="border no_border_left">Posisi / Jabatan</td>
                                <td style="width: 9%" class="border no_border_left">'.ucwords(lang('pelkat')).'</td>
                            </tr>';
                        $no = 1;
                        foreach ($konten['jemaat'] as $jemaat){
                            echo '<tr style="text-align: center">
                                        <td class="border no_border_top">'.$no.'</td>
                                        <td class="border no_border_left no_border_top">'.$jemaat->nama_status_perkawinan.'</td>
                                        <td class="border no_border_left no_border_top">'.($jemaat->tgl_kawin_gereja ? reformat_date(balik_tanggal($jemaat->tgl_kawin_gereja)) : '-').'</td>
                                        <td class="border no_border_left no_border_top">'.($jemaat->tgl_kawin_sipil ? reformat_date(balik_tanggal($jemaat->tgl_kawin_sipil)) : '-').'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->golongan_darah).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->nama_jenjang_pendidikan).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->gelar).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->jurusan_pendidikan).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->nama_pekerjaan).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->posisi_jabatan).'</td>
                                        <td class="border no_border_left no_border_top">'.coverMe($jemaat->singkatan_pelkat).'</td>
                                    </tr>';
                            $no++;
                        }
                        echo $additional_row;
                        ?>
                        </tbody>
                    </table>
                    <table style="width: 100%; margin-top: 20px">
                        <tr valign="top" style="text-align: center; font-weight: bold; font-size: 10px;">
                            <td style="text-align: left; width: 30%">
                                Dikeluarkan Tanggal: <?php echo date('d').' '.bulan((int) date('m')).' '.date('Y'); ?>
                            </td>
                            <td>
                                KEPALA KELUARGA
                                <br><br><br><br><br>
                                <div style="text-decoration: underline"><?php echo strtoupper($nama_kk); ?></div>
                                Tanda tangan/cap jempol
                            </td>
                            <td>
                                KOORDINATOR SEKTOR
                                <br><br><br><br><br>
                                <div style="text-decoration: underline"><?php echo strtoupper(($konten['jemaat'][0]->koordinator_sektor == '' ? '' : $konten['jemaat'][0]->koordinator_sektor )); ?></div>
                            </td>
                            <td>
                                <?php echo strtoupper(lang('ketua majelis jemaat').'<br>'.$konten['profil_gereja']->nama_gereja); ?>
                                <br><br><br><br>
                                <div style="text-decoration: underline"><?php echo strtoupper(($konten['profil_gereja']->kmj == '' ? '' : $konten['profil_gereja']->kmj )); ?></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            </tbody>
        </table>
    </div>


</div>
<?php $this->view('include/js'); ?>
<script>

</script>

</body>
</html>
