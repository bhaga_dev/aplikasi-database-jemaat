<html lang="id"><head>
    <title>Profil Jemaat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        @media print {
            body {
                padding: 0!important;
                margin: 0!important;
            }

            #action-area {
                display: none;
            }
        }

        @media screen and (min-width: 1025px) {
            .btn-download {
                display: none !important;
            }

            .btn-back {
                display: none !important;
            }
        }

        @media screen and (max-width: 1024px) {
            .content-area>div {
                width: auto !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 720px) {
            .content-area>div {
                width: auto !important;
            }
        }

        @media screen and (max-width: 420px) {
            .content-area>div {
                width: 790px !important;
            }
        }

        @media screen and (max-width: 430px) {
            .content-area {
                transform: scale(0.59) translate(-35%, -35%)
            }

            .content-area>div {
                width: 720px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 380px) {
            .content-area {
                transform: scale(0.45) translate(-58%, -62%);
            }

            .content-area>div {
                width: 790px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 320px) {
            .content-area>div {
                width: 700px !important;
            }
        }

        .tabel_data{
            font-size: 13px;
        }
        .header {
            font-weight: bold; font-size: 15px; padding-top: 20px;
        }
    </style>

<body id="lembar_invoice" style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">

    <div id="action-area">
        <div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 35px;">
            </div>
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">

                <a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #7400C8;border-radius: 8px;background-color: #7400C8;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
                </a>
            </div>
        </div>
        <div id="extwaiokist" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW=="><div id="extwaiimpotscp" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW==" vn="0yten"></div></div>
    </div>

<div class="content-area">

    <div style="margin: auto; width: 790px;">
        <table style="width: 100%; padding: 25px 32px;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>
                    <!-- header -->

                    <table width="100%">
                        <tbody>
                        <tr>
                            <td style="text-align: left;">
                                <div style="font-weight: bold; font-size: 23px;">Profil Jemaat</div>
                            </td>
                            <td style="text-align: right;">
                                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" alt="<?php echo $aplikasi; ?>" style="margin-top: -23px;" width="150px">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table class="tabel_data">
                        <tr>
                            <td colspan="3" class="header">Biodata Jemaat</td>
                        </tr>
                        <tr>
                            <td>Kode Jemaat</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->kode_jemaat; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_jemaat; ?></td>
                        </tr>
                        <tr>
                            <td>Hubungan Keluarga</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_hubungan_keluarga; ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo jenis_kelamin($konten['jemaat']->jenis_kelamin); ?></td>
                        </tr>
                        <tr>
                            <td>Tempat & Tgl. Lahir</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->tempat_lahir, 'Tidak diketahui').' / '.($konten['jemaat']->tgl_lahir ? reformat_date(balik_tanggal($konten['jemaat']->tgl_lahir)) : 'Tidak diketahui'); ?></td>
                        </tr>
                        <tr>
                            <td>Golongan Darah</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->golongan_darah); ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->alamat, 'Tidak ada alamat'); ?></td>
                        </tr>
                        <tr>
                            <td>RT / RW</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->rt, '000').' / '.coverMe($konten['jemaat']->rw, '000'); ?></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_kelurahan; ?></td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_kecamatan; ?></td>
                        </tr>
                        <tr>
                            <td>Kota / Kabupaten</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_kabupaten_kota; ?></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo $konten['jemaat']->nama_provinsi; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Data Baptis</td>
                        </tr>
                        <tr>
                            <td>Status Baptis</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->status_baptis == 'S' ? 'Sudah Baptis' : 'Belum Baptis'); ?></td>
                        </tr>
                        <tr>
                            <td>Tempat Baptis</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->tempat_baptis); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Baptis</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->tgl_baptis ? reformat_date(balik_tanggal($konten['jemaat']->tgl_baptis)) : '-'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Data Sidi</td>
                        </tr>
                        <tr>
                            <td>Status Sidi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->status_sidi == 'S' ? 'Sudah Sidi' : 'Belum Sidi'); ?></td>
                        </tr>
                        <tr>
                            <td>Tempat Sidi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->tempat_sidi); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Sidi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->tgl_sidi ? reformat_date(balik_tanggal($konten['jemaat']->tgl_sidi)) : '-'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Data Perkawinan</td>
                        </tr>
                        <tr>
                            <td>Status Perkawinan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->nama_status_perkawinan, 'Tidak diketahui'); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kawin Gereja</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->tgl_kawin_gereja ? reformat_date(balik_tanggal($konten['jemaat']->tgl_kawin_gereja)) : '-'); ?></td>
                        </tr>
                        <tr>
                            <td>Tempat Kawin Sipil</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->tgl_kawin_sipil ? reformat_date(balik_tanggal($konten['jemaat']->tgl_kawin_sipil)) : '-'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Pendidikan</td>
                        </tr>
                        <tr>
                            <td>Jenjang Pendidikan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->nama_jenjang_pendidikan); ?></td>
                        </tr>
                        <tr>
                            <td>Jurusan Pendidikan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->jurusan_pendidikan); ?></td>
                        </tr>
                        <tr>
                            <td>Gelar</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->gelar); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Pekerjaan</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Saat Ini</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->nama_pekerjaan); ?></td>
                        </tr>
                        <tr>
                            <td>Kantor / Institusi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->kantor_institusi); ?></td>
                        </tr>
                        <tr>
                            <td>Posisi / Jabatan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->posisi_jabatan); ?></td>
                        </tr>
                        <tr>
                            <td>Profesi</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->profesi); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Kontak</td>
                        </tr>
                        <tr>
                            <td>Telp. Rumah</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->telepon_rumah); ?></td>
                        </tr>
                        <tr>
                            <td>Nomor HP</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->nomor_hp); ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->email); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Pengalaman & Keahlian</td>
                        </tr>
                        <tr>
                            <td>Pengalaman Gereja</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->pengalaman_gereja); ?></td>
                        </tr>
                        <tr>
                            <td>Penguasaan Bahasa</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->penguasaan_bahasa); ?></td>
                        </tr>
                        <tr>
                            <td>Keahlian</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->keahlian); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header">Lain-Lain</td>
                        </tr>
                        <tr>
                            <td>Riwayat</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->riwayat); ?></td>
                        </tr>
                        <tr>
                            <td>Catatan</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo coverMe($konten['jemaat']->catatan); ?></td>
                        </tr>
                        <tr>
                            <td>Status Jemaat</td>
                            <td style="width: 5%;">:</td>
                            <td><?php echo ($konten['jemaat']->status_jemaat == 1 ? 'Aktif' : 'Tidak Aktif'); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>

            </tbody>
        </table>
    </div>


</div>
<?php $this->view('include/js'); ?>
<script>

</script>

</body>
</html>
