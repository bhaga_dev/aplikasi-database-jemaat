<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Ringkasan
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="row">
                        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Anggota Jemaat Beradasarkan <?php echo ucwords(lang('pelkat')); ?>
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget16">
                                        <div class="kt-widget16__stats">
                                            <div class="kt-widget16__visual">
                                                <div id="jemaat_pelkat" style="height: 200px; width: 200px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px dashed #d8d8d8; width: 100%;">
                                    <div>
                                        <table class="table" id="jemaat_pelkat_tabel">
                                            <thead>
                                            <tr>
                                                <td>legend</td>
                                                <td><?php echo ucwords(lang('pelkat')); ?></td>
                                                <td>Jumlah jemaat</td>
                                                <td>Persentase</td>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Jumlah Kepala Keluarga <?php echo ucwords(lang('sektor')); ?>
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget16">
                                        <div class="kt-widget16__stats">
                                            <div class="kt-widget16__visual">
                                                <div id="kk_sektor" style="height: 200px; width: 200px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px dashed #d8d8d8; width: 100%;">
                                    <div>
                                        <table class="table" id="kk_sektor_tabel">
                                            <thead>
                                            <tr>
                                                <td>Legend</td>
                                                <td><?php echo ucwords(lang('sektor')); ?></td>
                                                <td>Jumlah KK</td>
                                                <td>Persentase</td>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Jumlah Anggota Keluarga <?php echo ucwords(lang('sektor')); ?>
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget16">
                                        <div class="kt-widget16__stats">
                                            <div class="kt-widget16__visual">
                                                <div id="anggota_keluarga_sektor" style="height: 200px; width: 200px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px dashed #d8d8d8; width: 100%;">
                                    <div>
                                        <table class="table" id="anggota_kk_tabel">
                                            <thead>
                                            <tr>
                                                <td>Legend</td>
                                                <td><?php echo ucwords(lang('sektor')); ?></td>
                                                <td>Jumlah Jemaat</td>
                                                <td>Persentase</td>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Jumlah Jemaat Berdasarkan Jenis Kelamin
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget16">
                                        <div class="kt-widget16__stats">
                                            <div class="kt-widget16__visual">
                                                <div id="jenis_kelamin" style="height: 200px; width: 200px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px dashed #d8d8d8; width: 100%;">
                                    <div>
                                        <table class="table" id="jenis_kelamin_tabel">
                                            <thead>
                                            <tr>
                                                <td>Legend</td>
                                                <td><?php echo ucwords(lang('sektor')); ?></td>
                                                <td>Jumlah Jemaat</td>
                                                <td>Persentase</td>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Jumlah Jemaat Berdasarkan Usia
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget16">
                                        <div class="kt-widget16__stats">
                                            <div class="kt-widget16__visual">
                                                <div id="jemaat_berdasarkan_usia" style="height: 200px; width: 200px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px dashed #d8d8d8; width: 100%;">
                                    <div>
                                        <table class="table" id="jemaat_usia_tabel">
                                            <thead>
                                            <tr>
                                                <td>Legend</td>
                                                <td>Usia</td>
                                                <td>Jml. Jemaat</td>
                                                <td>Persentase</td>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    function morris_donut(element, data, colors){
        Morris.Donut({
            element: element,
            data: data,
            // labelColor: '#7400C8',
            colors: colors
            //formatter: function (x) { return x + "%"}
        });
    }

    function jemaat_pelkat_load(){
        preloader('show');
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/summary/jemaat_pelkat',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');
                var colors = [];
                var total_jemaat = 0;
                for(var i = 0; i < data.length; i++){
                    if(data[i].label == 'PA'){
                        var warna = '#68B265';
                    }
                    else if(data[i].label == 'PT'){
                        var warna = '#F7EE05';
                    }
                    else if(data[i].label == 'GP'){
                        var warna = '#0002F7';
                    }
                    else if(data[i].label == 'PKB'){
                        var warna = '#5A5957';
                    }
                    else if(data[i].label == 'PKP'){
                        var warna = '#371C63';
                    }
                    else if(data[i].label == 'PKLU'){
                        var warna = '#F17519';
                    }
                    else{
                        var warna = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');
                    }
                    colors[i] = warna;
                    total_jemaat += parseInt(data[i].value);
                }
                morris_donut('jemaat_pelkat', data, colors);

                var data_detail = '';

                for(var i = 0; i < data.length; i++){
                    var persen = Math.round((data[i].value / total_jemaat) * 100);

                    data_detail += '<tr>' +
                            '<td><div style="background-color: '+colors[i]+'; height: 10px; width: 100%; border-radius: 7px;"></div></td>' +
                            '<td>'+data[i].label+'</td>' +
                            '<td>'+data[i].value+'</td>' +
                            '<td>'+persen+' %</td>' +
                        '</tr>';
                }

                data_detail += '<tr style="font-weight: bold">' +
                    '<td colspan="2" style="text-align: right">TOTAL</td>' +
                    '<td>'+total_jemaat+'</td>' +
                    '<td></td>' +
                    '</tr>';
                $("#jemaat_pelkat_tabel tbody").html(data_detail);


            }
        })
    }
    jemaat_pelkat_load();

    function kk_sektor_load(){
        preloader('show');
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/summary/kk_sektor',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');
                var colors = [];
                var total_jemaat = 0;
                for(var i = 0; i < data.length; i++){
                    colors[i] = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

                    total_jemaat += parseInt(data[i].value);
                }
                morris_donut('kk_sektor', data, colors);

                var data_detail = '';
                for(var i = 0; i < data.length; i++){
                    var persen = Math.round((data[i].value / total_jemaat) * 100);
                    data_detail += '<tr>' +
                        '<td><div style="background-color: '+colors[i]+'; height: 10px; width: 100%; border-radius: 7px;"></div></td>' +
                            '<td>'+data[i].label+'</td>' +
                            '<td>'+data[i].value+'</td>' +
                            '<td>'+persen+' %</td>' +
                        '</tr>';
                }

                data_detail += '<tr style="font-weight: bold;">' +
                                    '<td style="text-align: right" colspan="2">TOTAL</td>' +
                                    '<td>'+total_jemaat+'</td>' +
                                    '<td></td>' +
                                '</tr>';
                $("#kk_sektor_tabel tbody").html(data_detail);


            }
        })
    }
    kk_sektor_load();

    function anggota_keluarga_sektor_load(){
        preloader('show');
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/summary/anggota_keluarga_sektor',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');
                var colors = [];
                var total_jemaat = 0;
                for(var i = 0; i < data.length; i++){
                    colors[i] = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

                    total_jemaat += parseInt(data[i].value);
                }
                morris_donut('anggota_keluarga_sektor', data, colors);

                var data_detail = '';
                for(var i = 0; i < data.length; i++){
                    var persen = Math.round((data[i].value / total_jemaat) * 100);

                    data_detail += '<tr>' +
                            '<td><div style="background-color: '+colors[i]+'; height: 10px; width: 100%; border-radius: 7px;"></div></td>' +
                            '<td>'+data[i].label+'</td>' +
                            '<td>'+data[i].value+'</td>' +
                            '<td>'+persen+' %</td>' +
                        '</tr>';
                }

                data_detail += '<tr style="font-weight: bold;">' +
                    '<td style="text-align: right" colspan="2">TOTAL</td>' +
                    '<td>'+total_jemaat+'</td>' +
                    '<td></td>' +
                    '</tr>';
                $("#anggota_kk_tabel tbody").html(data_detail);

            }
        })
    }
    anggota_keluarga_sektor_load();

    function jenis_kelamin_load(){
        preloader('show');
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/summary/jns_kelamin',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');
                var colors = [];
                var total_jemaat = 0;
                for(var i = 0; i < data.length; i++){
                    // colors[i] = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

                    total_jemaat += parseInt(data[i].value);

                    if(data[i].label == 'Laki-Laki'){
                        var warna = '#2234cb';
                    }
                    else if(data[i].label == 'Perempuan'){
                        var warna = '#D0448F';
                    }
                    colors[i] = warna;
                }
                morris_donut('jenis_kelamin', data, colors);

                var data_detail = '';
                for(var i = 0; i < data.length; i++){
                    var persen = Math.round((data[i].value / total_jemaat) * 100);
                    data_detail += '<tr>' +
                        '<td><div style="background-color: '+colors[i]+'; height: 10px; width: 100%; border-radius: 7px;"></div></td>' +
                        '<td>'+data[i].label+'</td>' +
                        '<td>'+data[i].value+'</td>' +
                        '<td>'+persen+' %</td>' +
                        '</tr>';
                }

                data_detail += '<tr style="font-weight: bold;">' +
                    '<td style="text-align: right" colspan="2">TOTAL</td>' +
                    '<td>'+total_jemaat+'</td>' +
                    '<td></td>' +
                    '</tr>';
                $("#jenis_kelamin_tabel tbody").html(data_detail);

            }
        })
    }
    jenis_kelamin_load();

    function jemaat_berdasarkan_usia(){
        preloader('show');
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/summary/jemaat_berdasarkan_usia',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');
                var colors = [];
                var total_jemaat = 0;

                var list = [
                    'Bayi ( 0 - 1 Thn )',
                    'Anak-Anak ( 2 - 10 Thn )',
                    'Remaja ( 11 - 19 Thn )',
                    'Dewasa ( 20 - 60 Thn )',
                    'Lanjut Usia ( > 60 Thn )',
                    'Tidak Diketahui',
                ];
                for(var i = 0; i < data.length; i++){
                    colors[i] = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

                    total_jemaat += parseInt(data[i].value);
                }
                morris_donut('jemaat_berdasarkan_usia', data, colors);

                var data_detail = '';
                for(var i = 0; i < data.length; i++){
                    var label = data[i].label;
                    for(var a = 0; a < list.length; a++){
                        if(list[a].includes(data[i].label)){
                            label = list[a];
                        }
                    }

                    var persen = Math.round((data[i].value / total_jemaat) * 100);
                    data_detail += '<tr>' +
                                        '<td><div style="background-color: '+colors[i]+'; height: 10px; width: 100%; border-radius: 7px;"></div></td>' +
                                        '<td>'+label+'</td>' +
                                        '<td>'+data[i].value+'</td>' +
                                        '<td>'+persen+' %</td>' +
                                    '</tr>';
                }

                data_detail += '<tr style="font-weight: bold;">' +
                                    '<td style="text-align: right" colspan="2">TOTAL</td>' +
                                    '<td>'+total_jemaat+'</td>' +
                                    '<td></td>' +
                                '</tr>';
                $("#jemaat_usia_tabel tbody").html(data_detail);


            }
        })
    }
    jemaat_berdasarkan_usia();

</script>
</body>
</html>
