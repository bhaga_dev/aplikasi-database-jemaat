<html lang="id"><head>
    <title>Ulang Tahun Kelahiran</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        @media print {
            body {
                padding: 0!important;
                margin: 0!important;
            }

            #action-area {
                display: none;
            }
        }

        @media screen and (min-width: 1025px) {
            .btn-download {
                display: none !important;
            }

            .btn-back {
                display: none !important;
            }
        }

        @media screen and (max-width: 1024px) {
            .content-area>div {
                width: auto !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 720px) {
            .content-area>div {
                width: auto !important;
            }
        }

        @media screen and (max-width: 420px) {
            .content-area>div {
                width: 790px !important;
            }
        }

        @media screen and (max-width: 430px) {
            .content-area {
                transform: scale(0.59) translate(-35%, -35%)
            }

            .content-area>div {
                width: 720px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 380px) {
            .content-area {
                transform: scale(0.45) translate(-58%, -62%);
            }

            .content-area>div {
                width: 790px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 320px) {
            .content-area>div {
                width: 700px !important;
            }
        }
    </style>

<body id="lembar_invoice" style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">

    <div id="action-area">
        <div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 35px;">
            </div>
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">
                <a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #7400C8;border-radius: 8px;background-color: #7400C8;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
                </a>
            </div>
        </div>
        <div id="extwaiokist" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW=="><div id="extwaiimpotscp" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW==" vn="0yten"></div></div>
    </div>

<div class="content-area">

    <div style="margin: auto; width: 790px;">
        <table style="width: 100%; padding: 25px 32px;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>
                    <!-- header -->
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td style="text-align: left;">
                                <div style="font-weight: bold; font-size: 18px;">Laporan Ulang Tahun Kelahiran</div>
                                <div style="font-size: 14px;">Periode: <?php echo $konten['periode_awal'].' hingga '.$konten['periode_akhir']; ?></div>
                            </td>
                            <td style="text-align: right;">
                                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" alt="<?php echo $aplikasi; ?>" style="margin-top: -23px;" width="150px">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>


            <!-- ringkasan belanja -->
            <tr>
                <td>
                    <table style="border: thin solid #979797; border-radius: 4px; color: #343030; margin-top: 20px;" width="100%" cellspacing="0" cellpadding="10">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Jemaat</th>
                                <th>Nama Jemaat</th>
                                <th>Tanggal Lahir</th>
                                <th>Usia Kelahiran</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 13px;">
                            <?php
                            if($konten['data']->num_rows() > 0){
                                $no = 1;
                                $background = ['#FFFFFF', '#ededed'];

                                foreach($konten['data']->result() as $row){
                                    $tgl_lahir = explode('-', $row->tgl_lahir);
                                    $usia = date('Y') - $tgl_lahir[0];
                                    $tgl_lahir_format = $tgl_lahir[2].' '.bulan((int) $tgl_lahir[1]).' '.$tgl_lahir[0];

                                    $i = $no % 2;
                                    echo '<tr style="background-color: '.$background[$i].'">
                                        <td>'.$no.'</td>
                                        <td style="text-align: center">'.$row->kode_jemaat.'</td>
                                        <td style="text-align: center">'.$row->nama_jemaat.'</td>
                                        <td style="text-align: center">'.$tgl_lahir_format.'</td>
                                        <td style="text-align: center">'.$usia.' Tahun</td>
                                    </tr>';
                                    $no++;
                                }
                            }
                            else{
                                echo '<tr><td colspan="5" style="text-align: center">Tidak ada data</td></tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </td>
            </tr>

            </tbody>
        </table>
    </div>


</div>
<?php $this->view('include/js'); ?>
<script>

</script>

</body>
</html>
