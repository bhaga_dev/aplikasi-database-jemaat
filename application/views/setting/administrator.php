<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Administrator</h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <button class="btn btn-primary m-btn m-btn--custom m-btn--icon tabel_zone" onclick="form_action('show')">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Tambah Administrator
                                        </span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone hidden" id="form_administrator">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Administrator
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_administrator" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>administrator/simpan" method="post" autocomplete="off">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group row">
                                            <label class="<?php echo $kolom_label; ?> col-form-label">Nama admin <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <input type="hidden" class="form-control" id="id_admin" name="id_admin" autocomplete="off">
                                                <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama lengkap administrator" autocomplete="off">

                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <label for="username" class="<?php echo $kolom_label; ?> col-form-label">Username <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Masukan username administrator" autocomplete="off">

                                            </div>
                                        </div>

                                        <div id="password_area">
                                            <div class="form-group m-form__group row">
                                                <label for="password" class="<?php echo $kolom_label; ?> col-form-label">Sandi <?php echo $red_star; ?></label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukan sandi administrator" autocomplete="off">

                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="password_ulang" class="<?php echo $kolom_label; ?> col-form-label">Ulangi sandi <?php echo $red_star; ?></label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" id="password_ulang" name="password_ulang" placeholder="Ulangi sandi administrator" autocomplete="off">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <label for="password_ulang" class="<?php echo $kolom_label; ?> col-form-label">Tipe admin <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" data-placeholder="Pilih tipe administrator" id="tipe_admin" name="tipe_admin">
                                                    <?php
                                                    $jns_admin = $konten['jns_admin'];
                                                    if($jns_admin->num_rows() > 0){
                                                        foreach($jns_admin->result() as $data_jns_admin){
                                                            echo '<option value="'.$data_jns_admin->id_jns_admin.'">'.$data_jns_admin->jns_admin.'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="password_ulang" class="<?php echo $kolom_label; ?> col-form-label">Status admin <?php echo $red_star; ?></label>
                                            <div class="col-sm-9">
                                                <div class="kt-radio-inline">
                                                    <label class="kt-radio kt-radio--success">
                                                        <input type="radio" name="status_admin" id="aktif" value="A" checked> <label for="aktif">Aktif</label>
                                                        <span></span>
                                                    </label>
                                                    <label class="kt-radio kt-radio--success">
                                                        <input type="radio" name="status_admin" id="tidak_aktif" value="N"> <label for="tidak_aktif">Tidak Aktif</label>
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group m-form__group row">
                                            <label for="password_ulang" class="<?php echo $kolom_label; ?> col-form-label">Foto </label>
                                            <div class="col-sm-9">
                                                <input type="file" name="foto_admin" id="foto_admin" class="form-control" accept="image/x-png,image/gif,image/jpeg" onchange="display_gambar()">
                                                <img src="" id="img_viewer" name="img_viewer" class="m--img-rounded mt-3 m--img-centered" style="object-fit: cover; width: 150px; height: 150px;" <?php echo $no_avatar; ?>>
                                                <input type="hidden" id="foto_admin_blob" name="foto_admin_blob">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                        </button>
                                        <button class="btn btn-secondary waves-effect waves-light" type="button" onclick="form_action('hide')">
                                                <span class="btn-label"><i class="la la-times"></i>
                                                </span>Batal
                                        </button>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tabel_zone" id="tabel_administrator">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data administrator
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-sm table-striped" id="data_administrator">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Avatar</th>
                                        <th>Nama admin</th>
                                        <th>Username</th>
                                        <th>Tipe admin</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    function display_gambar() {
        $("#simpan").attr('disabled', 'disabled');
        generate_img('#foto_admin', '#img_viewer', function(result){
            $("#foto_admin_blob").val(result);
            $("#img_viewer").show(200);
            $("#simpan").removeAttr('disabled');
        });
    }

    function form_action(action){
        //reset form...
        $("#input_form_administrator")[0].reset();
        $("#username, #tipe_admin, [name='status_admin']").removeAttr('disabled');
        $("#img_viewer").attr('src', '<?php echo base_url($no_avatar_url); ?>');
        $("#password_area").show();
        if(action == 'show'){
            $(".form_zone").fadeIn(300);
            $(".tabel_zone").hide();
        }
        else{
            $(".form_zone").hide();
            $(".tabel_zone").fadeIn(300);
        }
    }

    var list_data;
    $("#input_form_administrator").on('submit', function(e){
        e.preventDefault();
        var id_admin = $("#id_admin").val();
        var nama_lengkap = $("#nama_lengkap").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var password_ulang = $("#password_ulang").val();
        var tipe_admin = $("#tipe_admin").val();
        var foto_admin = $("#foto_admin_blob").val();
        var status_admin = $('input[name=status_admin]:checked').val();
        var action = $("#action").val();

        if(nama_lengkap == '' || username == '' || password == '' || password_ulang == '' || tipe_admin == '' || status_admin == ''){
            <?php echo alert('kosong'); ?>
        }
        else if(password != password_ulang){
            <?php echo alert('password_tidak_sama'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');


                    if(data.sts == 1){
                        //load data..
                        load_data();

                        $("#input_form_administrator")[0].reset();
                        $("#action").val('save');
                        <?php echo alert('simpan_berhasil'); ?>
                        form_action('hide');
                    }
                    else if(data.sts == 'username_available'){
                        <?php echo alert('username_available'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }

                }
            });
        }

    });

    function edit(index){
        form_action('show');
        var data = list_data[index];
        $("#id_admin").val(data.id_admin);
        $("#nama_lengkap").val(data.nama_admin);
        $("#username").val(data.username_admin);
        $("#tipe_admin").val(data.id_jns_admin);
        if(data.status_admin == 'A')
            $("#aktif").prop("checked", true);
        else
            $("#tidak_aktif").prop("checked", true);
        $("#action").val('update');

        $("#img_viewer")
            .on('error', function(){
                $(this).attr('src', '<?php echo base_url($no_avatar_url); ?>');
            })
            .attr('src', '<?php echo base_url('page/generate_image'); ?>?width=150&height=150&path='+data.foto_admin);

        //hidden password...
        $("#password_area").hide();
        //manipulasi password textbox agar tidak dianggap kosong..
        //value yang dipasang di textbox tidak akan digunakan untuk update password..
        $("#password").val('x');
        $("#password_ulang").val('x');

        //set fokus pada jenis admin...
        $("#nama_lengkap").focus();

        if(data.id_admin == 1){
            $("#username, #tipe_admin, [name='status_admin']").attr('disabled', 'disabled');
        }
    }
    function hapus(index){
        var data = list_data[index];
        var pertanyaan = "Apakah anda yakin ingin menghapus data "+data.nama_admin+"?";

        konfirmasi(pertanyaan, function(){
            proses_hapus(data.id_admin);
        });
    }
    function proses_hapus(id){
        //show loading animation...
        preloader('show');
        $("#pertanyaan").modal('hide');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_admin'] = id;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>administrator/hapus',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('hapus_berhasil'); ?>
                }
                else if(data.sts == 'tidak_berhak'){
                    <?php echo alert('tidak_berhak_hapus_data'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }


            }

        });
    }
    function reset_pwd(index){
        var data = list_data[index];
        var pertanyaan = "Password akan direset menjadi sama dengan username. " +
            "Apakah anda yakin ingin mereset sandi "+data.nama_admin+"?";

        konfirmasi(pertanyaan, function(){
            proses_reset(data.id_admin, data.username_admin);
        });

    }
    function proses_reset(id, username){
        //show loading animation...
        preloader('show');
        $("#pertanyaan").modal('hide');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_admin'] = id;
        data['username'] = username;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>administrator/reset_password',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('reset_password_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }


            }

        });
    }
    function load_data(){
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';

        elementLoading('show', '#data_administrator');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>administrator/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //parse JSON...
                list_data = safelyParseJSON(msg);
                var rangkai = '';
                if(list_data.length > 0){
                    for(var i=0; i < list_data.length; i++){
                        var hapus = '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="hapus('+i+')" title="Hapus data"><i class="la la-trash"></i></button>';
                        if(list_data[i].id_admin == 1)
                            hapus = '';
                        rangkai += '<tr>' +
                            '<td>'+(i+1)+'.</td>' +
                            '<td>' +
                                '<div class="kt-user-card-v2">' +
                                    '<div class="kt-user-card-v2__pic">' +
                                        '<img src="<?php echo base_url(); ?>page/generate_image?width=70&height=70&path='+list_data[i].foto_admin+'" style="object-fit: cover;" <?php echo $no_avatar_for_js; ?>>'+
                                    '</div>' +
                                '</div>' +
                            '</td>' +
                            '<td>'+list_data[i].nama_admin+'</td>' +
                            '<td>'+list_data[i].username_admin+'</td>' +
                            '<td>'+list_data[i].jns_admin+'</td>' +
                            '<td>'+render_status_label(list_data[i].status_admin)+'</td>' +
                            '<td>' +
                                '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="reset_pwd('+i+')" title="Reset sandi"><i class="la la-unlock-alt"></i></button>'+
                                '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="edit('+i+')" title="Edit data"><i class="la la-edit"></i></button>'+
                            hapus+
                            '</td>' +
                            '</tr>';
                    }
                }

                if(rangkai){
                    $("#empty_state").remove();
                    $("#data_administrator").show();
                    $("#data_administrator tbody").html(rangkai);
                }
                else{
                    create_empty_state("#data_administrator");
                }
                elementLoading('hide', '#data_administrator');
            }

        });
    }
    load_data();

</script>

</body>
<!-- end::Body -->
</html>
